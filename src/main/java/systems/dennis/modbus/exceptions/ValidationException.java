package systems.dennis.modbus.exceptions;

public class ValidationException extends RuntimeException {
    public ValidationException(String s) {
        super(s);
    }
}
