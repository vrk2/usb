package systems.dennis.modbus.exceptions;

public class LinkAppWasNotCreatedRemotelyException extends Exception {
    public LinkAppWasNotCreatedRemotelyException(){
        super("Error on creating remote app.");
    }
}
