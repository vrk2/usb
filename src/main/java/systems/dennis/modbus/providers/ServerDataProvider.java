package systems.dennis.modbus.providers;

import org.springframework.ui.Model;
import systems.dennis.modbus.model.DeviceType;
import systems.dennis.modbus.model.Server;
import systems.dennis.modbus.repo.DeviceTypeRepo;
import systems.dennis.modbus.repo.ServerRepo;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.DataProvider;
import systems.dennis.shared.pojo_form.ItemValue;

import java.util.ArrayList;

public class ServerDataProvider implements DataProvider<String> {

    @Override
    public ArrayList<ItemValue<String>> getItems(WebContext.LocalWebContext context) {
        var repo = context.getBean(ServerRepo.class);
        var res = repo.findAll();

        var items = new ArrayList<ItemValue<String>>();
        for (Server server : res){
            items.add(new ItemValue<>(server.getName() + " [" + server.getUserName() + "]", server.getId()));
        }
        return items;
    }


}
