package systems.dennis.modbus.providers;

import systems.dennis.modbus.model.DeviceType;
import systems.dennis.modbus.service.DeviceTypeService;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_view.DefaultDataConverter;
import systems.dennis.modbus.model.Device;

public class DeviceTypeConvertor implements DefaultDataConverter<Integer, Device> {
    @Override
    public String convert(Integer object, Device data, WebContext.LocalWebContext context) {
        var resolver = context.getBean(DeviceTypeService.class);
        return resolver.findById((long) data.getDeviceType().getId()).orElseGet(DeviceType:: new).getName();
    }
}
