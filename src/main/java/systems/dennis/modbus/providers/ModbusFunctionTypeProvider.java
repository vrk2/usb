package systems.dennis.modbus.providers;

import org.springframework.ui.Model;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.DataProvider;
import systems.dennis.shared.pojo_form.ItemValue;
import systems.dennis.modbus.executors.ExecutorFactory;

import java.util.ArrayList;

public class ModbusFunctionTypeProvider implements DataProvider<String> {

    @Override
    public ArrayList<ItemValue<String>> getItems(WebContext.LocalWebContext context) {
        return ExecutorFactory.getExecutors(context);
    }

    public static String getById(long id, WebContext.LocalWebContext context){
        for (ItemValue<String> value : ExecutorFactory.getExecutors(context)){
            if (value.getValue() == id){
                return value.getLabel();

            }
        }
        throw new IllegalArgumentException("No FynctionProvider for type :" + id);
    }
}
