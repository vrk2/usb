package systems.dennis.modbus.providers;

import org.springframework.ui.Model;
import systems.dennis.modbus.repo.DeviceTypeRepo;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.DataProvider;
import systems.dennis.shared.pojo_form.ItemValue;
import systems.dennis.modbus.model.DeviceType;

import java.util.ArrayList;

public class DeviceTypeDataProvider implements DataProvider<String> {

    @Override
    public ArrayList<ItemValue<String>> getItems(WebContext.LocalWebContext context) {
        var repo = context.getBean(DeviceTypeRepo.class);
        var res = repo.findAll();

        var items = new ArrayList<ItemValue<String>>();
        for (DeviceType deviceType : res){
            items.add(new ItemValue<>(deviceType.getName(), deviceType.getId()));
        }
        return items;
    }


}
