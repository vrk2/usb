package systems.dennis.modbus.providers;

import systems.dennis.modbus.model.DeviceTemplate;
import systems.dennis.modbus.model.DeviceType;
import systems.dennis.modbus.service.DeviceTypeService;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_view.DefaultDataConverter;

public class DeviceTemplateTypeConvertor implements DefaultDataConverter<Integer, DeviceTemplate> {
    @Override
    public String convert(Integer object, DeviceTemplate data, WebContext.LocalWebContext context) {
        var resolver = context.getBean(DeviceTypeService.class);
        return resolver.findById((long) data.getDeviceType()).orElseGet(DeviceType:: new).getName();
    }
}
