package systems.dennis.modbus.forms;

import lombok.Data;
import systems.dennis.modbus.model.USBModusModel;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.modbus.validators.NonExisitingUsbNameValidator;
import systems.dennis.shared.pojo_form.*;
import systems.dennis.shared.pojo_view.*;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.pojo_view.list.Remote;
import systems.dennis.shared.validation.ValueIsIntAndMoreThenZero;
import systems.dennis.shared.validation.ValueNotEmptyValidator;


import jakarta.persistence.Transient;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.CHECKBOX;

@Data

@PojoListView(actions = {"new",  "settings"}, enableTitle = true)
public class USBModusModelForm implements DefaultForm {

    @PojoFormElement(type = DEFAULT_TYPES.HIDDEN)
    @PojoListViewField(searchable = false, visible = false)
    private Long id;

    @Validation({NonExisitingUsbNameValidator.class, ValueNotEmptyValidator.class})
    
    @PojoFormElement(required = true)
    @PojoListViewField( searchable = true)
    private String name;

    @PojoFormElement(required = true)
    @PojoListViewField( searchable = true,  widthInTable = "200px")
    private String parity;

    @PojoListViewField(searchable = true ,widthInTable = "150px", remote = @Remote (searchType = "integer"))
    @Validation(ValueIsIntAndMoreThenZero.class)
    @PojoFormElement(required = true , type = "number")
    private Integer dataBits;

    @Validation(ValueIsIntAndMoreThenZero.class)
    @PojoFormElement(required = true, type =  "number")

    @PojoListViewField(searchable = true, remote = @Remote( searchType = "integer"), widthInTable = "150px")
    private Integer baudRate;

    @PojoFormElement(required = true, type = "number")
    @Validation (ValueIsIntAndMoreThenZero.class)
    @PojoListViewField(searchable = true, widthInTable = "150px",  remote = @Remote (searchType = "integer"))
    private Integer stopBids;

    @PojoListViewField(searchable = true, widthInTable = "150px", type = CHECKBOX, remote = @Remote (searchType = CHECKBOX))
    @PojoFormElement(type = CHECKBOX , checked = @Checkable(isCheckElement = true, checked = true))
    private Boolean isPRCmode;

    @Transient @PojoFormElement(visible = false)
    @PojoListViewField(searchable = false, sortable = false, showContent = false, widthInTable = "150px" , actions = {
             @UIAction(component = "edit"), @UIAction(component = "delete"), @UIAction(component = "new_device_form")
    })
    private Integer action;


    public static USBModusModelForm from(USBModusModel object) {
        var form = new USBModusModelForm();
        form.setId(object.getId());
        form.setName(object.getName());
        form.setBaudRate(object.getBaudRate());
        form.setDataBits(object.getDataBits());
        form.setParity(object.getParity());
        form.setIsPRCmode(object.getIsPRCmode());
        form.setStopBids(object.getStopBids());
        return form;
    }

    @Override
    public String asValue() {
        return name;
    }
}
