package systems.dennis.modbus.forms;

import lombok.Data;
import systems.dennis.modbus.providers.DeviceTypeDataProvider;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.modbus.providers.DeviceTemplateTypeConvertor;
import systems.dennis.shared.pojo_view.*;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;


import jakarta.persistence.Transient;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.DROP_DOWN;

@Data
@PojoListView()
public class DeviceTemplateForm implements DefaultForm {

    @PojoFormElement(type = DEFAULT_TYPES.HIDDEN)
    private Long id;

    @PojoFormElement( required = true)
    private String templateName;


    @PojoFormElement(type = DROP_DOWN, dataProvider = DeviceTypeDataProvider.class)
    @PojoListViewField(dataConverter = DeviceTemplateTypeConvertor.class)
    private Integer deviceType;

    @Transient @PojoFormElement(visible = false) @PojoListViewField(searchable = false, sortable = false, showContent = false, actions = {
            @UIAction(component = "link_to_device", parameters = {@UIActionParameter(key = "path",
                    value = "/device_templates"),
                    @UIActionParameter(key = "delete_ref", value = "device_template"),
                    @UIActionParameter(key = "path_device", value = "/device_templates/operations")})
    })
    private Integer actions;
}
