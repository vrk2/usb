package systems.dennis.modbus.forms;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.validation.ValueValidator;
import systems.dennis.shared.pojo_form.ValidationResult;

public class RemoteIsOn implements ValueValidator<DeviceOperationForm, Boolean> {
    @Override
    public ValidationResult validate(Class cl, DeviceOperationForm element, String field, Boolean value, boolean edit, WebContext.LocalWebContext context) {

       return ValidationResult.PASSED;
    }
}
