package systems.dennis.modbus.forms;

import lombok.Data;
import systems.dennis.modbus.model.DeviceType;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.PojoFormElement;

import systems.dennis.shared.pojo_view.UIAction;
import systems.dennis.shared.pojo_view.UIActionParameter;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.validation.ValueNotEmptyValidator;


import jakarta.persistence.Transient;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;

@Data

@PojoListView(enableSearching = false, showId = true, actions = {"new", "settings"}, enableTitle = true)
public class DeviceTypeForm implements DefaultForm {
    @PojoFormElement(type = HIDDEN, ID = true)
    @PojoListViewField()
    private Long id;

    @Validation({ValueNotEmptyValidator.class})
    @PojoListViewField(searchable = true)
    @PojoFormElement(required = true)
    private String name;

    @Transient
    @PojoFormElement(visible = false)
    @PojoListViewField(sortable = false, searchable = true, showContent = false, actions = {@UIAction(
            component = "edit"
    ), @UIAction(component = "delete")})
    private Integer actions;


    @Override
    public String asValue() {
        return name;
    }
}
