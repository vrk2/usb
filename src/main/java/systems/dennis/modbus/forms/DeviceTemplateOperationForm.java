package systems.dennis.modbus.forms;

import lombok.Data;
import systems.dennis.modbus.validators.DefaultOperationNotWritableTemplate;
import systems.dennis.modbus.providers.ModbusFunctionTypeProvider;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.Checkable;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.validation.ValueIsIntAndMoreThenZero;


import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.*;

@Data

public class DeviceTemplateOperationForm implements DefaultForm {

    @PojoFormElement(type = HIDDEN)
    private Long id;
    @PojoFormElement(type = HIDDEN)
    private Long deviceTemplate;

    @Validation(ValueIsIntAndMoreThenZero.class)
    @PojoFormElement (required = true, order = 4)
    private Integer startRead;
    @Validation(ValueIsIntAndMoreThenZero.class)
    @PojoFormElement( required = true,  order = 5)
    private Integer count;
    @PojoFormElement( order = 6)
    private String formula;

    @PojoFormElement (type = DROP_DOWN, dataProvider = ModbusFunctionTypeProvider.class, required = true, order = 2)
    private Integer type;

    @PojoFormElement (required = true, order = 1, autocomplete = false)
    private String name;

    @Validation(DefaultOperationNotWritableTemplate.class)
    @PojoFormElement (type = CHECKBOX , checked = @Checkable(isCheckElement = false), order = 8)
    private boolean defaultDeviceOperation;

}
