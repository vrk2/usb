package systems.dennis.modbus.forms;

import lombok.Data;
import org.springframework.beans.BeanUtils;
import systems.dennis.modbus.validators.DefaultOperationNotWritable;
import systems.dennis.modbus.validators.ReverseOperationSupported;
import systems.dennis.modbus.providers.ModbusFunctionTypeProvider;
import systems.dennis.modbus.service.DeviceService;
import systems.dennis.shared.annotations.ObjectByIdPresentation;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.Checkable;
import systems.dennis.shared.pojo_form.PojoFormElement;

import systems.dennis.modbus.model.DeviceOperation;

import systems.dennis.shared.pojo_view.UIAction;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.pojo_view.list.Remote;
import systems.dennis.shared.utils.bean_copier.DataTransformer;
import systems.dennis.shared.utils.bean_copier.IdToObjectTransformer;
import systems.dennis.shared.validation.ValueIsInt;
import systems.dennis.shared.validation.ValueIsIntAndMoreThenZero;


import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.*;

@Data
@PojoListView(actions = {"new", "settings"})
public class DeviceOperationForm implements DefaultForm {

    @PojoFormElement(type = HIDDEN)
    private Long id;

    @ObjectByIdPresentation
    @PojoFormElement (type = OBJECT_SEARCH, remote = @Remote(searchType = OBJECT_SEARCH, fetcher = "device", searchName = "device", searchField = "deviceName"))
    @PojoListViewField(searchable = true, remote = @Remote(searchType = OBJECT_SEARCH, fetcher = "device", searchName = "device", searchField = "deviceName"))
    @DataTransformer(transFormWith = IdToObjectTransformer.class, additionalClass = DeviceService.class)
    private Long device;

    @PojoFormElement (type = DROP_DOWN, dataProvider = ModbusFunctionTypeProvider.class, required = true)
    private Integer type;

    @Validation(ValueIsInt.class)
    @PojoFormElement( required = true)
    private Integer startRead;
    @Validation(ValueIsIntAndMoreThenZero.class)
    @PojoFormElement( required = true)
    private Integer count;
    private String formula;

    @PojoFormElement (required = true)
    private String name;

    @Validation(DefaultOperationNotWritable.class)
    @PojoFormElement (type = CHECKBOX , checked = @Checkable(isCheckElement = false) )
    @PojoListViewField (type = CHECKBOX)
    private Boolean defaultOperation;

    @Validation(RemoteIsOn.class)
    @PojoFormElement (type = CHECKBOX , remote=@Remote(searchType = CHECKBOX) , checked = @Checkable(isCheckElement = false))
    @PojoListViewField(type = "CHECKBOX")
    private Boolean remoteEnable;

    @Validation(ReverseOperationSupported.class)
    @PojoFormElement (type = CHECKBOX , remote=@Remote(searchType = CHECKBOX), checked = @Checkable(isCheckElement = false) )
    @PojoListViewField (available = false)
    private Boolean createReverseOperation;


    @PojoFormElement(type = HIDDEN)
    @PojoListViewField (available = false)
    private Long remoteId;


    public static DeviceOperationForm from(DeviceOperation object) {
        DeviceOperationForm form = new DeviceOperationForm();
        BeanUtils.copyProperties(object, form);
        form.setDevice(object.getDevice().getId());
        return form;
    }

    @PojoListViewField (sortable = false, showContent = false, actions = {@UIAction(component = "edit"), @UIAction(component = "delete")})
    public Integer actions;

    @Override
    public String asValue() {
        return name;
    }
}
