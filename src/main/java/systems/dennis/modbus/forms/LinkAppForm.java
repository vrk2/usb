package systems.dennis.modbus.forms;

import lombok.Data;
import systems.dennis.modbus.validators.CentralServerAndCodeValidator;
import systems.dennis.modbus.providers.ServerDataProvider;
import systems.dennis.modbus.service.ServerService;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.Checkable;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.pojo_view.list.Remote;
import systems.dennis.shared.utils.bean_copier.DataTransformer;
import systems.dennis.shared.utils.bean_copier.IdToObjectTransformer;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.*;


@Data
public class LinkAppForm implements DefaultForm {


    public LinkAppForm(){}

    private String name;
    @PojoFormElement (type = HIDDEN)
    private String selfCode;
    @PojoFormElement(type = CHECKBOX, checked = @Checkable(checked = true))
    Boolean on;

    @PojoFormElement (type = HIDDEN)
    private Long id;


    @DataTransformer(transFormWith = IdToObjectTransformer.class, additionalClass = ServerService.class)
    @Validation(CentralServerAndCodeValidator.class)
    @PojoFormElement (type = OBJECT_SEARCH, remote = @Remote(searchType = OBJECT_SEARCH, fetcher = "servers", searchName = "server", searchField = "name"))
    @PojoListViewField(searchable = true, remote = @Remote(searchType = OBJECT_SEARCH, fetcher = "servers", searchName = "server", searchField = "name"))
    private Long central;

    @DataTransformer(transFormWith = IdToObjectTransformer.class, additionalClass = ServerService.class)
    @PojoFormElement (type = OBJECT_SEARCH, remote = @Remote(searchType = OBJECT_SEARCH, fetcher = "servers", searchName = "server", searchField = "name"))
    @PojoListViewField(searchable = true, remote = @Remote(searchType = OBJECT_SEARCH, fetcher = "servers", searchName = "server", searchField = "name"))
    private Long virtual;


    @PojoFormElement(type =  CHECKBOX, checked = @Checkable(checked = true))
    Boolean active;
}
