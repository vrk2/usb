package systems.dennis.modbus.forms;

import lombok.Data;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.pojo_view.UIAction;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.validation.ValueNotEmptyValidator;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.HIDDEN;
import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.PASSWORD;


@Data
@PojoListView(enableSearching = false, actions = {"new", "settings"})
public class ServerForm implements DefaultForm {

    @PojoListViewField (searchable = true)
    @PojoFormElement(type = HIDDEN, ID=true)
    private Long id;

    @PojoListViewField(searchable = true)
    @Validation(ValueNotEmptyValidator.class)
    @PojoFormElement(required = true)
    private String name;

    @Validation(ValueNotEmptyValidator.class)
    @PojoListViewField (searchable = true)
    @PojoFormElement(required = true)
    private String path;

    @PojoListViewField (searchable = true)
    @PojoFormElement
    private String userName;

    @PojoListViewField(visible = false)
    @PojoFormElement(type = PASSWORD)
    private String password;


    @PojoListViewField(showContent = false, sortable = false, actions = {
            @UIAction(component = "server-status")
    })


    @PojoFormElement(type = HIDDEN)
    private int status;

    @PojoListViewField(showContent = false, actions = {
            @UIAction(component = "edit"), @UIAction(component = "delete")
    })
    @PojoFormElement(type = HIDDEN)
    private int actions;

    @Override
    public String asValue() {
        return name;
    }
}
