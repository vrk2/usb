package systems.dennis.modbus.forms;

import lombok.Data;
import systems.dennis.modbus.service.DeviceTypeService;
import systems.dennis.modbus.service.UsbModusService;
import systems.dennis.modbus.validators.NotExistingUsbDeviceValidator;
import systems.dennis.shared.annotations.ObjectByIdPresentation;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.pojo_form.Checkable;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.pojo_view.DEFAULT_TYPES;
import systems.dennis.shared.pojo_view.UIAction;
import systems.dennis.shared.pojo_view.list.PojoListView;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.pojo_view.list.Remote;
import systems.dennis.shared.utils.bean_copier.DataTransformer;
import systems.dennis.shared.utils.bean_copier.IdToObjectTransformer;
import systems.dennis.shared.validation.ValueNotEmptyValidator;

import static systems.dennis.shared.pojo_view.DEFAULT_TYPES.*;

@Data
@PojoListView (actions = {"new", "new_operation", "settings"})
public class DeviceForm implements DefaultForm {

    @PojoFormElement (type = DEFAULT_TYPES.HIDDEN)
    private Long id;


    @PojoFormElement (type = OBJECT_SEARCH, remote = @Remote(searchType = OBJECT_SEARCH, fetcher = "usbmodel", searchName = "usb", searchField = "name"))
    @PojoListViewField(searchable = true, remote = @Remote(searchType = OBJECT_SEARCH, fetcher = "usbmodel", searchName = "usb", searchField = "name"))
    @ObjectByIdPresentation
    @DataTransformer(transFormWith = IdToObjectTransformer.class, additionalClass = UsbModusService.class)
    private Long model;

    @Validation(ValueNotEmptyValidator.class)
    @PojoListViewField(searchable = true, remote = @Remote(searchType = OBJECT_SEARCH, searchField = "name", searchName = "devicetype", fetcher = "devicetype"))
    @PojoFormElement (type = OBJECT_SEARCH, remote = @Remote(searchType = OBJECT_SEARCH, searchField = "name", searchName = "devicetype", fetcher = "devicetype"), required = true)
    @DataTransformer(transFormWith = IdToObjectTransformer.class, additionalClass = DeviceTypeService.class)
    @ObjectByIdPresentation
    private Long deviceType;

    @Validation({NotExistingUsbDeviceValidator.class, ValueNotEmptyValidator.class})
    @PojoFormElement( required = true, type = "number")
    @PojoListViewField (remote = @Remote(searchType = INTEGER))
    private Long deviceId;





    @Validation(ValueNotEmptyValidator.class)
    @PojoListViewField(searchable = true)
    @PojoFormElement (autocomplete = false , required = true)
    private String deviceName;


    @PojoFormElement
    @PojoListViewField (visible = false)
    private String path;
    @PojoFormElement ()
    @PojoListViewField ( visible = false)
    private String pin;

    @PojoFormElement (remote = @Remote (searchType = CHECKBOX), checked = @Checkable, type = CHECKBOX)
    @PojoListViewField (searchable = true, remote = @Remote(searchType = CHECKBOX), type = CHECKBOX)

    public boolean virtual;




    @PojoFormElement (visible = false)
    @PojoListViewField (actions = {@UIAction(component = "edit"), @UIAction(component = "delete"), @UIAction(component = "new_operation_device") , @UIAction(component = "operations")})
    private Integer actions;



    public boolean isVirtual(){
        return  virtual && path != null;
    }

    @Override
    public String asValue() {
        return deviceName;
    }
}
