package systems.dennis.modbus.service;

import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.service.AddEditFormService;
import systems.dennis.shared.service.PaginationService;
import systems.dennis.modbus.model.DeviceTemplate;
import systems.dennis.modbus.forms.DeviceTemplateForm;
import systems.dennis.modbus.repo.DeviceTemplateOperationRepo;
import systems.dennis.modbus.repo.DeviceTemplateRepo;

@Service
@DataRetrieverDescription(model = DeviceTemplate.class, form = DeviceTemplateForm.class, repo = DeviceTemplateRepo.class)
public class DeviceTemplateService extends PaginationService<DeviceTemplate> implements AddEditFormService< DeviceTemplate> {

    public DeviceTemplateService( WebContext context) {
        super(context);

    }




    @Override
    public void delete(Long id) throws ItemNotUserException, ItemNotFoundException {
        var item = getRepository().findById(id).orElseThrow(()-> new ItemNotFoundException(id) );
        var service = getContext().getBean(DeviceTemplateOperationRepo.class);
        var operations = service.getDeviceOperations(id);
        service.deleteAll(operations);
        getRepository().delete(item);
    }

}
