package systems.dennis.modbus.service;

import org.springframework.stereotype.Service;
import systems.dennis.modbus.model.DeviceType;
import systems.dennis.modbus.repo.DeviceTypeRepo;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.service.AddEditFormService;
import systems.dennis.shared.service.PaginationService;
import systems.dennis.modbus.forms.DeviceTypeForm;

@Service
@DataRetrieverDescription(model = DeviceType.class, form = DeviceTypeForm.class, repo = DeviceTypeRepo.class)
public class DeviceTypeService extends PaginationService<DeviceType> implements
        AddEditFormService<DeviceType> {



    public DeviceTypeService(WebContext context) {
        super(context);
    }

    public String getText(Long deviceType) {
        return getRepository().findOne(QueryCase.equalsOf("id", deviceType).specification()).orElse(new DeviceType()).getName();
    }

}
