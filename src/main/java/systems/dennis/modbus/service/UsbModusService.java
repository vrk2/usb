package systems.dennis.modbus.service;

import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import systems.dennis.modbus.exceptions.ValidationException;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.model.USBModusModel;
import systems.dennis.modbus.forms.USBModusModelForm;
import systems.dennis.modbus.repo.UsbModusRepo;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.SearchEntityApi;
import systems.dennis.shared.controller.SearcherInfo;
import systems.dennis.shared.repository.DefaultSearchSpecification;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.service.AddEditFormService;
import systems.dennis.shared.service.PaginationService;


import java.util.Optional;

@Service
@DataRetrieverDescription(model = USBModusModel.class, form = USBModusModelForm.class, repo = UsbModusRepo.class)
public class UsbModusService extends PaginationService<USBModusModel> implements AddEditFormService< USBModusModel> {

    static  {
        SearchEntityApi.registerSearch("usb", new SearcherInfo("name", UsbModusService.class));
    }
    public UsbModusService( WebContext context) {
        super(context);
    }


    public USBModusModel preAdd(USBModusModel object) {
        if (object.getBaudRate() == 0) {
            object.setBaudRate(9600);
        }
        if (object.getDataBits() == 0) {
            object.setDataBits(8);
        }
        if (object.getStopBids() == 0) {
            object.setStopBids(1);
        }
        if (object.getName() == null) throw new ValidationException("name.empty");
        if (object.getParity() == null) {
            object.setParity("none");
        }


        return object;
    }

    @Override
    public boolean exists(USBModusModel object) {
        return getRepository().existsById(object.getId());
    }

    @Override
    public USBModusModel save(USBModusModel form)  {
        return getRepository().save(form);
    }



    public boolean existsByName(String value) {
        try {
            return getRepository().findOne(QueryCase.equalsOf("name", value).specification()).isPresent();
        } catch (IncorrectResultSizeDataAccessException exception) {
            //normally we should not have this point if the system is done from scratch. this means that we have
            //more than one result for this name and it means that we have item with the same name
            return true;
        }
    }

    public Optional<Device> findByDeviceId(Long deviceId) {
        QueryCase queryCase = QueryCase.equalsOf("id", deviceId);
        DefaultSearchSpecification<Device> deviceDefaultSearchSpecification = queryCase.specification();
        var res = getContext().getBean(DeviceService.class).find(deviceDefaultSearchSpecification, 0L, 1, 0);
        return res.isEmpty() ? Optional.empty() : Optional.of(res.getContent().get(0));
    }
}
