package systems.dennis.modbus.service;

import org.springframework.stereotype.Service;
import systems.dennis.modbus.model.Server;
import systems.dennis.modbus.forms.ServerForm;
import systems.dennis.modbus.repo.ServerRepo;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.service.AddEditFormService;
import systems.dennis.shared.service.PaginationService;

@Service
@DataRetrieverDescription(repo = ServerRepo.class, form = ServerForm.class, model = Server.class)
public class ServerService extends PaginationService<Server> implements AddEditFormService< Server> {
    public ServerService(WebContext holder) {
        super(holder);
    }
}
