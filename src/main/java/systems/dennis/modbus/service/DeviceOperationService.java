package systems.dennis.modbus.service;

import org.springframework.stereotype.Service;
import systems.dennis.modbus.client.CentralAppClient;
import systems.dennis.modbus.client.model.RemoteWritableForm;
import systems.dennis.modbus.executors.ExecutorFactory;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.forms.DeviceOperationForm;
import systems.dennis.modbus.providers.ModbusFunctionTypeProvider;
import systems.dennis.modbus.repo.DeviceOperationRepo;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.ItemForAddContainsIdException;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.UnmodifiedItemSaveAttempt;
import systems.dennis.shared.service.AddEditFormService;
import systems.dennis.shared.service.PaginationService;

import java.util.Optional;

@Service
@DataRetrieverDescription(repo = DeviceOperationRepo.class, model = DeviceOperation.class, form = DeviceOperationForm.class)
public class DeviceOperationService extends PaginationService<DeviceOperation> implements AddEditFormService< DeviceOperation> {

    private final CentralAppClient client;

    public DeviceOperationService(WebContext context, CentralAppClient client) {
        super(context);
        this.client = client;
    }


    @Override
    public DeviceOperationRepo getRepository() {
        return super.getRepository();
    }

    @Override
    public DeviceOperation afterAdd(DeviceOperation object) {
        if (object.getDefaultOperation()) {
            getRepository().setAllMainOperationsToFalse(object.getId());
            getRepository().save(object);
        }
        return super.afterAdd(object);
    }

    @Override
    public DeviceOperation preAdd(DeviceOperation object) throws ItemForAddContainsIdException {
        object.setHasParameter(ExecutorFactory.typeIsWritable(object.getType()));
        return super.preAdd(object);
    }

    public DeviceOperation findByRemoteId(Long id){
        return getRepository().findFirstByRemoteId(id).orElse(null);
    }

    @Override
    public DeviceOperation preEdit(DeviceOperation object, DeviceOperation original) throws UnmodifiedItemSaveAttempt, ItemNotFoundException {
        //todo multiple default impl possible when edit is failed / or none at all!
        //normally should be implemented afterAdd(method)
        if (object.getDefaultOperation()) {
            object.setHasParameter(false);
            getRepository().setAllMainOperationsToFalse(object.getId());
        }

        object.setReverseOperation(original.getReverseOperation());

        object.setHasParameter(ExecutorFactory.typeIsWritable(object.getType()));

        return super.preEdit(object, original);
    }






    public String getOperationTypeByName(Long id) {
        return ModbusFunctionTypeProvider.getById(id, getContext());
    }

    public Optional<DeviceOperation> findDefaultDeviceOperation(Long deviceId) {
        return getRepository().findDefaultDeviceOperation(deviceId);
    }

    public void export(DeviceOperation item) {
        RemoteWritableForm form = RemoteWritableForm.from(item);

        var res   = client.saveWritable(form);

        if (res != null && item.getRemoteId() == null){
            item.setRemoteId(res.getId());
            getRepository().save(item);
        }
    }



    public void updateReverseOperationId(Long id, Long id1) {
        getRepository().updateReverseOperationId(id, id1);
    }
}
