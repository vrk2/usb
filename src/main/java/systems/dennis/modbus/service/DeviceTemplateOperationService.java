package systems.dennis.modbus.service;

import org.springframework.stereotype.Service;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.UnmodifiedItemSaveAttempt;
import systems.dennis.shared.service.AddEditFormService;
import systems.dennis.shared.service.PaginationService;
import systems.dennis.modbus.executors.ExecutorFactory;
import systems.dennis.modbus.model.DeviceTemplateOperation;
import systems.dennis.modbus.forms.DeviceTemplateOperationForm;
import systems.dennis.modbus.repo.DeviceTemplateOperationRepo;

@Service
@DataRetrieverDescription(form = DeviceTemplateOperationForm.class, repo = DeviceTemplateOperationRepo.class, model = DeviceTemplateOperation.class)
public class DeviceTemplateOperationService extends PaginationService<DeviceTemplateOperation> implements AddEditFormService<DeviceTemplateOperation> {


    private DeviceTemplateService deviceService;

    public DeviceTemplateOperationService(DeviceTemplateOperationRepo repo, DeviceTemplateService deviceService, WebContext context) {
        super(context);
        this.deviceService = deviceService;
    }



    @Override
    public DeviceTemplateOperation afterAdd(DeviceTemplateOperation object) {
        if (object.getDefaultOperation()) {
            getRepository().setAllMainOperationsToFalse(object.getId());

            getRepository().save(object);
        }
        return super.afterAdd(object);
    }

    @Override
    public DeviceTemplateOperation preEdit(DeviceTemplateOperation object, DeviceTemplateOperation original) throws UnmodifiedItemSaveAttempt, ItemNotFoundException {
        //todo multiple default impl possible when edit is failed / or none at all!
        //normally should be implemented afterAdd(method)
        if (object.getDefaultOperation()) {
            object.setHasParameter(false);
            getRepository().setAllMainOperationsToFalse(object.getId());
        }

        object.setHasParameter(ExecutorFactory.typeIsWritable(object.getType()));
        return super.preEdit(object, original);

    }




    @Override
    public DeviceTemplateOperationRepo getRepository() {
        return super.getRepository();
    }



}
