package systems.dennis.modbus.service;

import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import systems.dennis.modbus.api.LinkAppController;
import systems.dennis.modbus.client.CentralAppClient;
import systems.dennis.modbus.exceptions.LinkAppWasNotCreatedRemotelyException;
import systems.dennis.modbus.model.LinkApp;
import systems.dennis.modbus.model.Server;
import systems.dennis.modbus.forms.LinkAppForm;
import systems.dennis.modbus.repo.LinkAppRepo;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.service.AddEditFormService;
import systems.dennis.shared.service.PaginationService;

@Service
@DataRetrieverDescription(repo = LinkAppRepo.class, model = LinkApp.class, form = LinkAppForm.class)
public class LinkAppService extends PaginationService<LinkApp> implements
        AddEditFormService<LinkApp> {


    private LinkApp app;

    public LinkAppService(LinkAppRepo repo, WebContext context) {
        super(context);

        app = repo.findFirstByActiveIsTrue().orElse(null);
    }


    public LinkApp getCurrentApp(){
        return app == null ?  getRepository().findFirstByActiveIsTrue().orElse(null) : app;
    }
    public String code(){
        return app.getSelfCode();
    }

    public void setCurrentApp(LinkApp app){
        this.app = app;
    }

    @Override
    public LinkAppRepo  getRepository() {
        return super.getRepository();
    }

    public boolean isAppInstalled(){
        return app != null;
    }

    public LinkApp findByServer(Server server) {
        return getRepository().findFirstByCentralAndSelfCodeNotNull(server).orElse(null);
    }

    @SneakyThrows
    @Override
    public LinkApp afterAdd(LinkApp object) {
        var res = getContext().getBean(CentralAppClient.class).registerApp(getBean(LinkAppController.class).toForm(object));
        object.setSelfCode(res.getSelfCode());
        if (object.getSelfCode() == null) {
            delete(object.getId());
            throw new LinkAppWasNotCreatedRemotelyException();
        } else {
            var service = getContext().getBean(LinkAppService.class);
            if (object.getActive() != null && object.getActive()) {
                var items = service.find();
                items.forEach(x -> {
                    if (object.getId() != x.getId()) {
                        x.setActive(false);
                        service.save(x);
                    }
                });

            }

            getRepository().save(object);
            if (object.getActive() != null && object.getActive()) {
                getContext().getBean(LinkAppService.class).setCurrentApp(object);
            }
        }
        return super.afterAdd(object);
    }
}
