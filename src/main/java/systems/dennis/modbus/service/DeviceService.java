package systems.dennis.modbus.service;

import org.springframework.stereotype.Service;
import systems.dennis.modbus.client.VirtualDeviceClient;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.repo.DeviceOperationRepo;
import systems.dennis.shared.annotations.DataRetrieverDescription;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.ItemForAddContainsIdException;
import systems.dennis.shared.service.AddEditFormService;
import systems.dennis.shared.service.PaginationService;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.forms.DeviceForm;
import systems.dennis.modbus.repo.DeviceRepo;

import java.util.List;

@Service
@DataRetrieverDescription(model = Device.class, form = DeviceForm.class, repo = DeviceRepo.class)
public class DeviceService extends PaginationService<Device> implements AddEditFormService< Device> {


    private UsbModusService usbModusService;

    public DeviceService( UsbModusService usbModusService, WebContext context) {
        super(context);
        this.usbModusService = usbModusService;
    }


    @Override
    public Device preAdd(Device object) throws ItemForAddContainsIdException {

        if (object.isVirtual()){
            try {
                getBean(VirtualDeviceClient.class).connect(object);
            } catch (Exception e){
                //cannot connect
                throw new RuntimeException("Cannot connect to virtual device");
            }

        }

        return super.preAdd(object);
    }

    @Override
    public DeviceRepo getRepository() {
        return super.getRepository();
    }

    public boolean existsByDeviceIdAndUsbModelId(Long deviceId, Long model) {
        return getRepository().existsByDeviceIdAndModel(deviceId, model);
    }

    public List<DeviceOperation> findOperations(Long deviceId) {
        return getContext().getBean(DeviceOperationRepo.class).findOperations(deviceId);
    }


}
