package systems.dennis.modbus.pages;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.model.USBModusModel;
import systems.dennis.modbus.forms.USBModusModelForm;
import systems.dennis.modbus.repo.DeviceRepo;
import systems.dennis.modbus.service.UsbModusService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebConstants;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.*;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;

import static systems.dennis.shared.config.WebConstants.*;

@RequestMapping("/modbus")
@Slf4j
@Controller
public class ModbusPage extends ApplicationContext {

    public ModbusPage(WebContext context) {
        super(context);

    }

    @GetMapping("")
    public String index(){
        return asPage("/modbus", "/index");
    }




}
