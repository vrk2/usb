package systems.dennis.modbus.pages;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import systems.dennis.modbus.model.DeviceType;
import systems.dennis.modbus.service.DeviceTypeService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.*;
import systems.dennis.modbus.forms.DeviceTypeForm;
import systems.dennis.shared.utils.ApplicationContext;

import static systems.dennis.shared.config.WebConstants.asPage;

@RequestMapping("/device_type")
@Slf4j
@Controller
public class DeviceTypePage extends ApplicationContext {


    public DeviceTypePage(WebContext context) {
        super(context);
    }

    @GetMapping("")
    public String index(){
        return asPage("/devicetype", "/index");
    }
}
