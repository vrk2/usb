package systems.dennis.modbus.pages;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import systems.dennis.modbus.model.DeviceFromTemplate;
import systems.dennis.shared.config.WebConstants;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.ItemForAddContainsIdException;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.DeviceTemplateOperation;
import systems.dennis.modbus.repo.DeviceRepo;
import systems.dennis.modbus.repo.DeviceTemplateOperationRepo;
import systems.dennis.modbus.service.DeviceOperationService;
import systems.dennis.modbus.service.DeviceTemplateService;
import systems.dennis.modbus.service.UsbModusService;

import static systems.dennis.shared.config.WebConstants.WEB_OBJECT_MODEL_ATTRIBUTE;
import static systems.dennis.shared.config.WebConstants.withMessage;

@Controller
@RequestMapping("/template")
@Slf4j
public class TemplatePage {

    private final UsbModusService service;
    private final DeviceTemplateService deviceTemplateService;
    private final DeviceRepo deviceRepo;
    private final DeviceOperationService deviceOperationService;
    private final WebContext.LocalWebContext context;
    private final DeviceTemplateOperationRepo deviceOperationRepo;

    public TemplatePage(UsbModusService service, WebContext context, DeviceTemplateService deviceTemplateService, DeviceRepo deviceRepo, DeviceOperationService deviceOperationService, DeviceTemplateOperationRepo deviceOperationRepo) {
        this.service = service;
        this.deviceTemplateService = deviceTemplateService;
        this.deviceRepo = deviceRepo;
        this.deviceOperationService = deviceOperationService;
        this.context = WebContext.LocalWebContext.of("pages.templates", context);
        this.deviceOperationRepo = deviceOperationRepo;
    }

    @GetMapping("/add-from-template/{usbId}")
    public String addDeviceFromTemplates(@RequestParam(required = false) String message,
                                        @PathVariable("usbId") Long usbId , Model model) throws ItemNotFoundException {
        service.findById(usbId).orElseThrow(() -> new ItemNotFoundException(" no device with id: " + usbId));
        var templates = deviceTemplateService.find();
        model.addAttribute("templates", templates);
        model.addAttribute(WEB_OBJECT_MODEL_ATTRIBUTE, new DeviceFromTemplate(usbId));
        return  WebConstants.asPage("/template", "/add-from-template");
    }

    @PostMapping("/add-from-template")
    public String addJob(@ModelAttribute DeviceFromTemplate form) {


        try {
            if (form.getUsbId() == null) {
                throw new IllegalArgumentException("Usb id should not be null");
            }
            if (form.getDeviceId() == null) {
                throw new IllegalArgumentException("Device number should not be null");
            }

            var res = createFromTemplate(form);

            return withMessage(context.getScoped("form.saved"), "redirect:/modbus/devices/" + res.getModel().getId() );

        } catch (Exception e) {
            log.error("Could not add job", e);
            context.setAttribute(WebConstants.WEB_OBJECT_MODEL_ATTRIBUTE, form);
            return  withMessage(e.getMessage(), "redirect:/template/add-from-template/" + form.getUsbId() );
        }
    }

    private Device createFromTemplate(DeviceFromTemplate form) throws ItemNotFoundException, ItemForAddContainsIdException {
        Device device = new Device();
        device.setDeviceId(form.getDeviceId().longValue());
        device.setDeviceName(form.getDeviceName());
        device.setModel(service.findById(form.getUsbId()).orElseThrow(()-> new ItemNotFoundException(form.getUsbId())));

        var savedDevice = deviceRepo.save(device);

        var operations = deviceOperationRepo.getDeviceOperations(form.getTemplateId());

        for (DeviceTemplateOperation operation: operations){
            DeviceOperation deviceOperation  = new DeviceOperation();
            BeanUtils.copyProperties(operation, deviceOperation);
            deviceOperation.setDevice(savedDevice);
            deviceOperationService.save(deviceOperation);
        }

        return device;
    }
}
