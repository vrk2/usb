package systems.dennis.modbus.pages;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import systems.dennis.modbus.forms.DeviceTemplateForm;
import systems.dennis.modbus.service.DeviceTemplateService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebConstants;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.*;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.modbus.model.DeviceTemplate;
import systems.dennis.modbus.model.DeviceTemplateOperation;
import systems.dennis.modbus.repo.DeviceTemplateOperationRepo;

import java.util.List;

import static systems.dennis.shared.config.WebConstants.*;

@RequestMapping("/device_templates")
@Slf4j
@Controller
@WebFormsSupport(DeviceTemplateService.class)
public class DeviceTemplatePage implements
        AddFormController< DeviceTemplate,DeviceTemplateForm>,
        EditFormController< DeviceTemplate, DeviceTemplateForm>,
        ListFormSupport<DeviceTemplate, DeviceTemplateForm> {

    private final DeviceTemplateService service;
    private final WebContext.LocalWebContext context;
    private final DeviceTemplateOperationRepo operationRepo;

    public DeviceTemplatePage(WebContext context, DeviceTemplateService service, DeviceTemplateOperationRepo operationRepo) {
        this.service = service;

        this.context = WebContext.LocalWebContext.of("device", context);
        this.operationRepo = operationRepo;
    }
    @GetMapping("/add/{deviceId}")
    public String addOperationToDevice(@RequestParam(required = false) String message, @PathVariable("deviceId") Long deviceId , Model model) throws ItemNotFoundException {

        service.findById(deviceId).orElseThrow(() -> new ItemNotFoundException(" no device with id: " + deviceId));
        prePageCheck(null);
        model.addAttribute("isEdit", false);

        loadDefaultFormData(model);
        return  WebConstants.asPage(getPath(), "/add");
    }

    @SneakyThrows
    @GetMapping("/operations/{device}")
    public String device(@PathVariable("device") Long deviceId, Model model){
        var device = service.findById(deviceId).orElseThrow(() -> new ItemNotFoundException(" no device with id: " + deviceId));


        QueryCase queryCase = QueryCase.equalsOf("id", deviceId);
        queryCase.setComplex(true);
        queryCase.setJoinOn("deviceTemplate");
        List<DeviceTemplateOperation> deviceList = operationRepo.findAll(queryCase.specification());
        model.addAttribute("items", deviceList);
        model.addAttribute("device", deviceId);

        model.addAttribute("deviceName", device.getTemplateName());
        return WebConstants.asPage(getPath(), "/operations");
    }


    @Override
    public WebContext.LocalWebContext getContext() {
        return context;
    }

    @Override
    public OnSaveListener<DeviceTemplate, DeviceTemplateForm> onSaveListener() {

        return new OnSaveListenerImpl<>(context, getPath(), log){
            @Override
            public String getOkPath(DeviceTemplate object, DeviceTemplateForm deviceTemplateForm) {
                return  asRedirect("", "/device_templates/operations/" + object.getId());
            }
        };
    }



}
