package systems.dennis.modbus.pages;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import systems.dennis.modbus.forms.DeviceTemplateOperationForm;
import systems.dennis.modbus.service.DeviceTemplateService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebConstants;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.*;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.pojo_form.ValidationContext;
import systems.dennis.modbus.model.DeviceTemplateOperation;
import systems.dennis.modbus.service.DeviceTemplateOperationService;

import static systems.dennis.shared.config.WebConstants.*;

@RequestMapping("/device_templates_operations")
@Slf4j
@Controller
@WebFormsSupport(DeviceTemplateOperationService.class)
public class DeviceTemplateOperationPage implements AddFormController< DeviceTemplateOperation, DeviceTemplateOperationForm>,
        EditFormController<DeviceTemplateOperation,DeviceTemplateOperationForm>,
        ListFormSupport<DeviceTemplateOperation, DeviceTemplateOperationForm> {

    private final WebContext.LocalWebContext context;

    public DeviceTemplateOperationPage(WebContext context) {

        this.context = WebContext.LocalWebContext.of("device", context);
    }
    @GetMapping("/add/{deviceID}")
    public String addDeviceToUsb(@RequestParam(required = false) String message, @PathVariable("deviceID") Long deviceID , Model model) throws ItemNotFoundException {
              prePageCheck(null);
        model.addAttribute("isEdit", false);

        model.addAttribute("deviceId", deviceID);
        loadDefaultFormData(model);
        DeviceTemplateOperationForm device = (DeviceTemplateOperationForm) model.getAttribute(WEB_OBJECT_MODEL_ATTRIBUTE);
        if (device != null){
            device.setDeviceTemplate(deviceID);
        }

        return WebConstants.asPage(getPath(), WebConstants.WEB_PAGE_ADD);
    }

    @Override
    public String addJob(@RequestParam(required = false) String message, Model model){
        throw new UnsupportedOperationException("Oparation should be linked to device");
    }

    @Override
    public WebContext.LocalWebContext getContext() {
        return context;
    }


    @Override
    public OnSaveListener<DeviceTemplateOperation, DeviceTemplateOperationForm> onSaveListener() {
        return new OnSaveListenerImpl<>(context, getPath(), log){
            @Override
            public String getOkPath(DeviceTemplateOperation object, DeviceTemplateOperationForm deviceTemplateOperationForm) {
                return  asRedirect("/device_templates", "/operations/" + object.getDeviceTemplate());
            }

            @Override
            public String getValidationErrorPath(boolean b, DeviceTemplateOperation o, DeviceTemplateOperationForm deviceTemplateOperationForm, ValidationContext context) {
                return asRedirect(getPath(), WEB_PAGE_ADD+ "/" + deviceTemplateOperationForm.getDeviceTemplate());
            }
        };

    }


    @SneakyThrows
    @Override
    public DeviceTemplateOperation fromForm(DeviceTemplateOperationForm item) {
        DeviceTemplateOperation deviceOpertion = new DeviceTemplateOperation();
        deviceOpertion.setType(item.getType());
        deviceOpertion.setStartRead(item.getStartRead());

        var device = context.getBean(DeviceTemplateService.class).findById(item.getDeviceTemplate()).orElseThrow(()-> new ItemNotFoundException(item.getId()));
        deviceOpertion.setDeviceTemplate(device);
        deviceOpertion.setCount(item.getCount());
        deviceOpertion.setId(item.getId());
        deviceOpertion.setFormula(item.getFormula());
        deviceOpertion.setName(item.getName());
        deviceOpertion.setDefaultOperation(item.isDefaultDeviceOperation());
        return deviceOpertion;
    }
}
