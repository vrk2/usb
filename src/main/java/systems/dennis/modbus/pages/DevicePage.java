package systems.dennis.modbus.pages;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.USBModusModel;
import systems.dennis.modbus.forms.DeviceForm;
import systems.dennis.modbus.service.DeviceService;
import systems.dennis.modbus.service.UsbModusService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebConstants;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.*;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.pojo_form.ValidationContext;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;

import static systems.dennis.shared.config.WebConstants.*;

@RequestMapping("/devices")
@Slf4j
@Controller
public class DevicePage extends ApplicationContext {


    public DevicePage(WebContext context) {
        super(context);
    }

    @GetMapping("")
    public String index(){
        return asPage("/devices", "/index");
    }

    @SneakyThrows
    @GetMapping("/operations/{device}")
    public String device(@PathVariable("device") Long deviceId, Model model) {
        Device device = getService().findById(deviceId, new ItemNotFoundException(" no device with id: " + deviceId));

        DeviceService service = getService();

        List<DeviceOperation> deviceList =  service.findOperations(deviceId);
        model.addAttribute("items", deviceList);
        model.addAttribute("device", deviceId);

        model.addAttribute("usb", device.getModel().getName());
        model.addAttribute("usbId", device.getModel().getId());
        model.addAttribute("deviceName", device.getDeviceName());
        return WebConstants.asPage("/devices", "/operations");
    }

    public DeviceService getService(){
        return getBean(DeviceService.class);
    }

}
