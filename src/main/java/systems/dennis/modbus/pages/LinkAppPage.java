package systems.dennis.modbus.pages;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import systems.dennis.modbus.client.CentralAppClient;
import systems.dennis.modbus.exceptions.LinkAppWasNotCreatedRemotelyException;
import systems.dennis.modbus.model.LinkApp;
import systems.dennis.modbus.forms.LinkAppForm;
import systems.dennis.modbus.service.LinkAppService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.AddFormController;
import systems.dennis.shared.controller.forms.EditFormController;
import systems.dennis.shared.controller.forms.OnSaveListener;
import systems.dennis.shared.controller.forms.OnSaveListenerImpl;

import static systems.dennis.shared.config.WebConstants.*;

@Controller
@RequestMapping("/link_app")
@Slf4j
@WebFormsSupport(LinkAppService.class)
public class LinkAppPage implements
        AddFormController<LinkApp, LinkAppForm>,
        EditFormController<LinkApp, LinkAppForm> {
    private final WebContext.LocalWebContext context;
    private final CentralAppClient centralAppClient;

    public LinkAppPage(WebContext context, CentralAppClient cetralAppClient) {
        this.context = WebContext.LocalWebContext.of("link.app", context);
        this.centralAppClient = cetralAppClient;
    }


    @Override
    public String addEdit(Long id, Model model) {
        return EditFormController.super.addEdit(id, model);
    }

    @Override
    public WebContext.LocalWebContext getContext() {
        return context;
    }

    @GetMapping("/list")
    public String list(Model model) {
        LinkAppService service = getService();
        model.addAttribute("items", service.find());
        return asPage(getPath(), WEB_API_LIST);
    }

    @Override
    public OnSaveListener<LinkApp, LinkAppForm> onSaveListener() {
        return new OnSaveListenerImpl<>(context, getPath(), log) {
            @SneakyThrows
            @Override
            public void extraAfterAdd(LinkApp object, LinkAppForm form) {



            }

            @SneakyThrows
            @Override
            public void onFail(LinkApp object, Exception e) {
                if (object.getId() != null) {
                    getService().delete(object.getId());
                }
            }
        };

    }

}
