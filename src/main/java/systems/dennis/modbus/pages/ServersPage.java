package systems.dennis.modbus.pages;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import systems.dennis.modbus.model.Server;
import systems.dennis.modbus.forms.ServerForm;
import systems.dennis.modbus.service.ServerService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.*;
import systems.dennis.shared.utils.ApplicationContext;

import static systems.dennis.shared.config.WebConstants.asPage;

@Controller
@RequestMapping("/servers")
@Slf4j
@WebFormsSupport(ServerService.class)
public class ServersPage  extends  ApplicationContext {

    public ServersPage(WebContext context) {
        super(context);
    }

    @GetMapping("")
    public String index(){
        return asPage("/servers", "/index" );
    }


}
