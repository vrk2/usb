package systems.dennis.modbus.pages;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import systems.dennis.shared.config.WebConstants;
import systems.dennis.modbus.service.UsbModusService;

@Slf4j
@Controller
@RequestMapping("/")
public class IndexController {
    private final UsbModusService service;

    public IndexController(UsbModusService service) {
        this.service = service;
    }

    @RequestMapping("/")
    public String index( Model model){

        model.addAttribute("items", service.find());

        return WebConstants.asPage("/index", "/index");
    }
}
