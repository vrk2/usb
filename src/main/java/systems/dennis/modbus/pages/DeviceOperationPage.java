package systems.dennis.modbus.pages;

import systems.dennis.modbus.executors.ExecutorFactory;
import systems.dennis.modbus.forms.DeviceOperationForm;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.controller.forms.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import systems.dennis.shared.config.WebConstants;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.pojo_form.ValidationContext;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.service.DeviceOperationService;
import systems.dennis.modbus.service.DeviceService;
import systems.dennis.shared.utils.bean_copier.BeanCopier;

import static systems.dennis.shared.config.WebConstants.*;

@RequestMapping("/operations")
@Slf4j
@Controller
@WebFormsSupport(DeviceOperationService.class)
public class DeviceOperationPage implements AddFormController< DeviceOperation, DeviceOperationForm>,
        EditFormController< DeviceOperation, DeviceOperationForm>,
        ListFormSupport<DeviceOperation, DeviceOperationForm> {

    private final WebContext.LocalWebContext context;
    private final DeviceService deviceService;

    public DeviceOperationPage(WebContext context, DeviceService usbModusService) {

        this.context = WebContext.LocalWebContext.of("device", context);
        this.deviceService = usbModusService;
    }

    @GetMapping("")
    public String index(){
        return asPage("/operations", "/index");
    }

    @GetMapping("/add/{deviceID}")
    public String addOperationToDevice(@RequestParam(required = false) String message, @PathVariable("deviceID") Long deviceID, Model model) throws ItemNotFoundException {
        var usb = deviceService.findById(deviceID).orElseThrow(() -> new ItemNotFoundException(" no usb with id: " + deviceID));
        prePageCheck(null);
        model.addAttribute("isEdit", false);

        model.addAttribute("deviceId", deviceID);
        loadDefaultFormData(model);
        DeviceOperationForm device = (DeviceOperationForm) model.getAttribute(WEB_OBJECT_MODEL_ATTRIBUTE);
        if (device != null) {
            device.setDevice(usb.getId());
        }
        return WebConstants.asPage(getPath(), WebConstants.WEB_PAGE_ADD);
    }

    @Override
    public String addJob(@RequestParam(required = false) String message, Model model) {
        throw new UnsupportedOperationException("Oparation should be linked to device");
    }

    @Override
    public WebContext.LocalWebContext getContext() {
        return context;
    }


    @Override
    public OnSaveListener<DeviceOperation, DeviceOperationForm> onSaveListener() {
        return new OnSaveListenerImpl<>(context, getPath(), log) {
            @SneakyThrows
            @Override
            public void extraAfterAdd(DeviceOperation object, DeviceOperationForm form) {
                var device = object.getDevice();
                DeviceOperationService service = getService();
                if (form.getCreateReverseOperation()) {
                    var reverseOperationType = ExecutorFactory.getReverseType(form.getType());
                    if (reverseOperationType != ExecutorFactory.NO_REVERSE) {
                        //we create a new type with the reverse type
                        if (form.getId() == null) {
                            DeviceOperation reverseOperation = fromForm(form);
                            reverseOperation.setType(reverseOperationType);
                            reverseOperation.setHasParameter(ExecutorFactory.typeIsWritable(reverseOperationType));
                            var res = service.save(reverseOperation);
                            service.updateReverseOperationId(res.getId(), object.getId());
                            if (res.isHasParameter()) {
                                service.export(res);
                            }
                        } else {
                            // we update reverse operation as well
                            DeviceOperation reverseOperation = service.findById(object.getReverseOperation().getId()).orElseThrow(() -> new ItemNotFoundException("Reverse operation could not be found for Object: " + form.getId()));
                            var updatedOperation = fromForm(form);
                            updatedOperation.setId(reverseOperation.getId());
                            updatedOperation.setType(ExecutorFactory.getReverseType(object.getType()));
                            updatedOperation.setHasParameter(ExecutorFactory.typeIsWritable(updatedOperation.getType()));
                            service.save(updatedOperation, true);

                            if (updatedOperation.isHasParameter()) {
                                service.export(updatedOperation);
                            }
                        }
                    }
                } else if (object.isHasParameter()) {
                    service.export(object);
                }
                super.extraAfterAdd(object, form);
            }

            @Override
            public String getOkPath(DeviceOperation object, DeviceOperationForm deviceOperationForm) {
                return asRedirect("/devices/", "operations/" + object.getDevice().getId());
            }

            @Override
            public String getValidationErrorPath(boolean b, DeviceOperation o, DeviceOperationForm deviceOperationForm, ValidationContext context) {
                return asRedirect(getPath(), "/add/" + deviceOperationForm.getDevice());
            }

            @Override
            public String getFailPath(Exception e, DeviceOperation object, DeviceOperationForm form) {
                return withMessage(e.getMessage(), asRedirect(getPath(), "/add/" + form.getDevice()));
            }
        };

    }



    @Override
    public DeviceOperation fromForm(DeviceOperationForm form) {

        var res = getContext().getBean(BeanCopier.class).copy(form, DeviceOperation.class);
        res.setHasParameter(ExecutorFactory.typeIsWritable(res.getType()));
        return res;
    }


    @Override
    public DeviceOperationForm toForm(DeviceOperation form) {

        return DeviceOperationForm.from(form);
    }
}
