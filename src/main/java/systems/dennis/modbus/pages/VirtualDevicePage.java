package systems.dennis.modbus.pages;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.USBModusModel;
import systems.dennis.modbus.forms.VirtualDeviceForm;
import systems.dennis.modbus.service.DeviceService;
import systems.dennis.modbus.service.UsbModusService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebConstants;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.forms.*;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.pojo_form.ValidationContext;
import systems.dennis.shared.utils.ApplicationContext;

import java.util.List;

import static systems.dennis.shared.config.WebConstants.*;

@RequestMapping("/virtual")
@Slf4j
@Controller
@WebFormsSupport(value = DeviceService.class, model = VirtualDeviceForm.class)
public class VirtualDevicePage extends ApplicationContext implements
        AddFormController< Device, VirtualDeviceForm>,
        EditFormController< Device,VirtualDeviceForm> {

    private final UsbModusService usbModusService;

    public VirtualDevicePage(WebContext context, UsbModusService usbModusService) {


        super(context);
        this.usbModusService = usbModusService;
    }

    @GetMapping("/add/{usbId}")
    public String addOperationToDevice(@RequestParam(required = false) String message, @PathVariable("usbId") Long usbId, Model model) throws ItemNotFoundException {
        USBModusModel usb = usbModusService.findById(usbId, new ItemNotFoundException(" no device with id: " + usbId));
        prePageCheck(null);
        model.addAttribute("isEdit", false);

        model.addAttribute("usb", usb.getName());
        loadDefaultFormData(model);
        VirtualDeviceForm deviceForm = (VirtualDeviceForm) model.getAttribute(WEB_OBJECT_MODEL_ATTRIBUTE);
//        deviceForm.setModel(usb.getId());
        return WebConstants.asPage(getPath(), "/add");
    }

    @SneakyThrows
    @GetMapping("/operations/{device}")
    public String device(@PathVariable("device") Long deviceId, Model model) {
        Device device = getService().findById(deviceId, new ItemNotFoundException(" no device with id: " + deviceId));

        DeviceService service = getService();

        List<DeviceOperation> deviceList =  service.findOperations(deviceId);
        model.addAttribute("items", deviceList);
        model.addAttribute("device", deviceId);

        model.addAttribute("usb", device.getModel().getName());
        model.addAttribute("usbId", device.getModel().getId());
        model.addAttribute("deviceName", device.getDeviceName());
        return WebConstants.asPage(getPath(), "/operations");
    }


    @Override
    public String addJob(@RequestParam(required = false) String message, Model model) {
        throw new UnsupportedOperationException("Device should be linked to usb");
    }

    @Override
    public OnSaveListener<Device, VirtualDeviceForm> onSaveListener() {

        return new OnSaveListenerImpl<>(getContext(), getPath(), log){
            @Override
            public String getOkPath(Device object, VirtualDeviceForm form) {
                return  withMessage(getContext().getScoped("form.saved"), asRedirect("/devices", "/operations/" + object.getId()));
            }

            @Override
            public String getValidationErrorPath(boolean b, Device o, VirtualDeviceForm deviceForm, ValidationContext context) {
                return asRedirect(getPath(),WEB_PAGE_ADD + "/" + o.getId() );
            }

            @Override
            public String getFailPath(Exception e, Device object, VirtualDeviceForm deviceForm) {
                return withMessage(e.getMessage(), asRedirect(getPath() , WEB_PAGE_ADD + "/" + object.getModel()));
            }
        };
    }

}
