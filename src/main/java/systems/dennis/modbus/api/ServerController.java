package systems.dennis.modbus.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.modbus.forms.ServerForm;
import systems.dennis.modbus.forms.USBModusModelForm;
import systems.dennis.modbus.model.Server;
import systems.dennis.modbus.model.USBModusModel;
import systems.dennis.modbus.service.ServerService;
import systems.dennis.modbus.service.UsbModusService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.SearchEntityApi;
import systems.dennis.shared.controller.SearcherInfo;
import systems.dennis.shared.controller.items.AddItemController;
import systems.dennis.shared.controller.items.DeleteItemController;
import systems.dennis.shared.controller.items.EditItemController;
import systems.dennis.shared.controller.items.ListItemController;
import systems.dennis.shared.utils.ApplicationContext;

@RestController ()
@Slf4j
@RequestMapping("/api/v2/servers")
@WebFormsSupport(ServerService.class)
public class ServerController extends ApplicationContext implements
        EditItemController<Server, ServerForm>,
        DeleteItemController,
        AddItemController<Server, ServerForm>,
        ListItemController<Server, ServerForm>  {

    static {
        SearchEntityApi.registerSearch("server", new SearcherInfo("name", ServerService.class));
    }
    public ServerController(WebContext context) {
        super(context);
    }
}
