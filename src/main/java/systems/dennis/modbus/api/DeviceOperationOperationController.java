package systems.dennis.modbus.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.items.AddItemController;
import systems.dennis.shared.controller.items.EditItemController;
import systems.dennis.shared.controller.items.ListItemController;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.forms.DeviceOperationForm;
import systems.dennis.modbus.service.DeviceOperationService;
import systems.dennis.modbus.service.DeviceService;
import systems.dennis.shared.utils.ApplicationContext;

@RestController
@Slf4j
@RequestMapping("/api/v2/device/operation")
@WebFormsSupport(DeviceOperationService.class)
public class DeviceOperationOperationController extends ApplicationContext implements
        AddItemController<DeviceOperation, DeviceOperationForm>,
        EditItemController<DeviceOperation, DeviceOperationForm>,
        ListItemController<DeviceOperation, DeviceOperationForm>  {

    private final DeviceService deviceService;

    public DeviceOperationOperationController(WebContext context, DeviceService deviceService) {
        super(context);
        this.deviceService = deviceService;
    }

}
