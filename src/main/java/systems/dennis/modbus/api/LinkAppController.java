package systems.dennis.modbus.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.modbus.forms.DeviceForm;
import systems.dennis.modbus.forms.LinkAppForm;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.model.LinkApp;
import systems.dennis.modbus.service.DeviceService;
import systems.dennis.modbus.service.LinkAppService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.SearchEntityApi;
import systems.dennis.shared.controller.SearcherInfo;
import systems.dennis.shared.controller.items.AddItemController;
import systems.dennis.shared.controller.items.DeleteItemController;
import systems.dennis.shared.controller.items.EditItemController;
import systems.dennis.shared.controller.items.ListItemController;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.utils.ApplicationContext;

@RestController
@RequestMapping("/api/v2/linkapp")
@WebFormsSupport(LinkAppService.class)
public class LinkAppController extends ApplicationContext implements
        AddItemController<LinkApp, LinkAppForm>,
        DeleteItemController,
        EditItemController<LinkApp, LinkAppForm>,
        ListItemController<LinkApp, LinkAppForm> {

    static {
        SearchEntityApi.registerSearch("linkapp", new SearcherInfo("name", LinkAppService.class ));
    }
    public LinkAppController(WebContext context) {
        super(context);
    }

    @Override
    public void delete(Long id) throws ItemNotUserException, ItemNotFoundException {
        getContext().getBean(DeleteController.class).deleteDevice(id);
    }

    @Override
    public String getDefaultField() {
        return "deviceName";
    }
}
