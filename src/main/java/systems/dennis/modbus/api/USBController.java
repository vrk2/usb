package systems.dennis.modbus.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.SearchEntityApi;
import systems.dennis.shared.controller.SearcherInfo;
import systems.dennis.shared.controller.items.AddItemController;
import systems.dennis.shared.controller.items.DeleteItemController;
import systems.dennis.shared.controller.items.EditItemController;
import systems.dennis.shared.controller.items.ListItemController;
import systems.dennis.modbus.model.USBModusModel;
import systems.dennis.modbus.forms.USBModusModelForm;
import systems.dennis.modbus.service.UsbModusService;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.utils.ApplicationContext;

@RestController ("UsbModelController")
@Slf4j
@RequestMapping("/api/v2/usbmodel")
@WebFormsSupport(UsbModusService.class)
public class USBController extends ApplicationContext implements
        EditItemController<USBModusModel, USBModusModelForm>,
        DeleteItemController,
        AddItemController<USBModusModel, USBModusModelForm>,
        ListItemController<USBModusModel, USBModusModelForm>  {

    static {
        SearchEntityApi.registerSearch("usbmodel", new SearcherInfo("name", UsbModusService.class));
    }
    public USBController(WebContext context) {
        super(context);

    }

//    @Override
//    public void delete(Long id) throws ItemNotUserException, ItemNotFoundException {
//        getContext().getBean(DeleteController.class).(id);
//    }


    @Override
    public USBModusModel fromForm(USBModusModelForm form) {
        return USBModusModel.from(form);
    }

    @Override
    public USBModusModelForm toForm(USBModusModel form) {
        return USBModusModelForm.from(form);
    }


}
