package systems.dennis.modbus.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.modbus.model.DeviceType;
import systems.dennis.modbus.service.DeviceTypeService;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.SearchEntityApi;
import systems.dennis.shared.controller.SearcherInfo;
import systems.dennis.shared.controller.items.AddItemController;
import systems.dennis.shared.controller.items.EditItemController;
import systems.dennis.shared.controller.items.ListItemController;
import systems.dennis.modbus.forms.DeviceTypeForm;
import systems.dennis.shared.utils.ApplicationContext;

@RestController
@RequestMapping("/api/v2/devicetype")
@Slf4j
@WebFormsSupport(DeviceTypeService.class)
public class DeviceTypeController extends ApplicationContext implements
        EditItemController<DeviceType, DeviceTypeForm>,
        AddItemController<DeviceType, DeviceTypeForm>,
        ListItemController<DeviceType, DeviceTypeForm>  {
    static {
        SearchEntityApi.registerSearch("devicetype", new SearcherInfo("name", DeviceTypeService.class));
    }

    public DeviceTypeController( WebContext context) {
        super(context);
    }

}
