package systems.dennis.modbus.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.service.*;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.controller.SearchEntityApi;
import systems.dennis.shared.controller.SearcherInfo;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.exceptions.ItemNotUserException;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.shared.utils.ApplicationContext;

@RestController
@RequestMapping("/api/v1")
@Slf4j
public class DeleteController extends ApplicationContext {

    private final DeviceOperationService deviceOperationService;
    private final DeviceService deviceService;
    private final DeviceTypeService deviceTypeService;
    private final DeviceTemplateOperationService deviceTemplateOperationService;
    private final DeviceTemplateService deviceTemplateService;

    static {
        SearchEntityApi.registerSearch("usb", new SearcherInfo("name", UsbModusService.class));
    }

    public DeleteController(WebContext context, final DeviceOperationService service, DeviceService deviceService, DeviceTypeService deviceTypeService,
                            DeviceTemplateOperationService deviceTemplateOperationService, DeviceTemplateService deviceTemplateService){
        super(context);

        this.deviceOperationService = service;
        this.deviceService = deviceService;
        this.deviceTypeService = deviceTypeService;
        this.deviceTemplateOperationService = deviceTemplateOperationService;
        this.deviceTemplateService = deviceTemplateService;
    }

    @PostMapping("/operations/delete/{id}")
    public void deleteOperation(@PathVariable  Long id) throws ItemNotUserException, ItemNotFoundException {

        var item = deviceOperationService.findById(id).orElseThrow(()-> new ItemNotFoundException(id));

        // delete related write operation
        deviceOperationService.delete(id);
        if (item.getReverseOperation()!= null){
            deviceOperationService.delete(item.getReverseOperation().getId());
        }

    }
    @PostMapping("/devicetype/delete/{id}")
    public void deleteDeviceType(@PathVariable  Long id) throws ItemNotUserException, ItemNotFoundException {
        deviceTypeService.delete(id);
    }
    @PostMapping("/servers/delete/{id}")
    public void deleteServerType(@PathVariable  Long id) throws ItemNotUserException, ItemNotFoundException {
        getBean(ServerService.class).delete(id);
    }
    @PostMapping("/device_template_operations/delete/{id}")
    public void deleteDeviceTemplateOperation(@PathVariable  Long id) throws ItemNotUserException, ItemNotFoundException {
        deviceTemplateOperationService.delete(id);
    }


    @PostMapping("/device_template/delete/{id}")
    public void deleteDeviceTemplate(@PathVariable  Long id) throws ItemNotUserException, ItemNotFoundException {
        deviceTemplateService.delete(id);
    }



    @PostMapping("/devices/delete/{id}")
    public void deleteDevice(@PathVariable  Long id) throws ItemNotUserException, ItemNotFoundException {
        QueryCase queryCase = QueryCase.equalsOf("id", id);
        queryCase.setComplex(true);
        queryCase.setJoinOn("device");
        var operations = deviceOperationService.getRepository().findAll(queryCase.specification());
        for (DeviceOperation x : operations) {
            deviceOperationService.delete(x.getId());
        }
        deviceService.delete(id);
    }
}
