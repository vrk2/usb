package systems.dennis.modbus.executors;


import com.ghgande.j2mod.modbus.msg.*;
import systems.dennis.modbus.Modbus;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

public class ReadRegisterExecutor extends AbstractExecutor {
    public static ReadRegisterExecutor INSTANCE = new ReadRegisterExecutor(null);
    public ReadRegisterExecutor(Object value) {
        super(value);
    }

    @Override
    public boolean accept(DeviceOperation deviceOperation) {
        return deviceOperation.getType().equals(Modbus.READ_REGISTER);
    }

    @Override
    public boolean isWriteExecutor() {
        return false;
    }


    @Override
    protected ExecutionResult readData(ModbusResponse response, DeviceOperation deviceOperation) {
        try {
            ReadInputRegistersResponse resp = (ReadInputRegistersResponse) response;
            return positive(resp.getRegisterValue(0));
        } catch (Exception e) {
            return fail(response, e);
        }

    }

    @Override
    public ModbusRequest createRequest(DeviceOperation operation) {
            return new ReadInputRegistersRequest(operation.getStartRead(), operation.getCount());
    }


}
