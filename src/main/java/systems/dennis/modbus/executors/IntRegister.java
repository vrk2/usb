package systems.dennis.modbus.executors;

import com.ghgande.j2mod.modbus.procimg.SimpleRegister;

public class IntRegister{
    private int value;

    public IntRegister(Object value){
        if (value == null){
            value = 0;
        }
        if (value instanceof Integer){
            this.value = (int) value;
            return;
        }
        try {
            this.value = Integer.parseInt(value.toString());
        } catch (Exception e){
            throw new IllegalArgumentException("Argument should be integer");
        }
    }
    public SimpleRegister[] toRegisters(){
        return new SimpleRegister[]{new SimpleRegister(value)};
    }
}
