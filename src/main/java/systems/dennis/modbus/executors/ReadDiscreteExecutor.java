package systems.dennis.modbus.executors;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.msg.*;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

public class ReadDiscreteExecutor extends AbstractExecutor {
    public static ReadDiscreteExecutor INSTANCE = new ReadDiscreteExecutor(null);
    public ReadDiscreteExecutor(Object value) {
        super(value);
    }

    @Override
    public boolean accept(DeviceOperation deviceOperation) {
        return deviceOperation.getType() == Modbus.READ_INPUT_DISCRETES;
    }

    @Override
    public boolean isWriteExecutor() {
        return false;
    }


    @Override
    protected ExecutionResult readData(ModbusResponse response, DeviceOperation deviceOperation) {
        try {
            ReadInputDiscretesResponse resp = (ReadInputDiscretesResponse) response;
            return positive(resp.getDiscreteStatus(0));
        } catch (Exception e) {
            return fail(response, e);
        }

    }

    @Override
    public ModbusRequest createRequest(DeviceOperation operation) {
        return new ReadInputDiscretesRequest(operation.getStartRead(),
                operation.getCount());
    }


}
