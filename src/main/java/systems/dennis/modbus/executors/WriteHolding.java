package systems.dennis.modbus.executors;

import com.ghgande.j2mod.modbus.msg.*;
import systems.dennis.modbus.Modbus;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

public class WriteHolding extends AbstractExecutor {
    public static WriteHolding INSTANCE = new WriteHolding(null);
    public WriteHolding(Object value) {
        super(value);
    }

    @Override
    public boolean accept(DeviceOperation deviceOperation) {
        return deviceOperation.getType() == Modbus.WRITE_HOLDING;
    }

    @Override
    public boolean isWriteExecutor() {
        return true;
    }


    @Override
    protected ExecutionResult readData(ModbusResponse response, DeviceOperation deviceOperation) {
        try {
            WriteMultipleRegistersResponse resp = (WriteMultipleRegistersResponse) response;
            return positive(applyFormula(toNormalString(resp.getMessage()), deviceOperation.getFormula()));
        } catch (Exception e) {
            return fail(response, e);
        }

    }

    @Override
    public Object adoptValue(Object initial) {
        if(initial == null) {
            return initial;
        }
        String stringVal = String.valueOf(initial);

        try{
            return Integer.parseInt(stringVal);
        } catch (Exception e){
            throw new IllegalArgumentException("Value must be integer");
        }
    }


    private Integer toNormalString(byte[] bytes) {
        var str = new StringBuffer();
        for (byte b : bytes) {
            str.append((int)b);
        }
        return  Integer.parseInt(str.toString());
    }


    @Override
    public ModbusRequest createRequest(DeviceOperation operation) {

        return new WriteMultipleRegistersRequest(operation.getStartRead(), new IntRegister(getValue()).toRegisters());
    }


}
