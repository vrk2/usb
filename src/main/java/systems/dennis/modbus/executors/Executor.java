package systems.dennis.modbus.executors;

import systems.dennis.modbus.controller.USBConnector;
import systems.dennis.shared.config.WebContext;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

public interface Executor {
    boolean accept(DeviceOperation deviceOperation);

    ExecutionResult execute(DeviceOperation deviceOperation,  WebContext.LocalWebContext context, USBConnector connector);

    boolean isWriteExecutor();

    default Object adoptValue(Object initial){
        return initial;
    }

}
