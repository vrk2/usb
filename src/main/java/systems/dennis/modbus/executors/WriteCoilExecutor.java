package systems.dennis.modbus.executors;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.msg.*;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

public class WriteCoilExecutor extends AbstractExecutor {
    public static WriteCoilExecutor INSTANCE = new WriteCoilExecutor(null);
    public WriteCoilExecutor(Object value) {

        super(value);
    }

    @Override
    public boolean accept(DeviceOperation deviceOperation) {
        return deviceOperation.getType().equals(Modbus.WRITE_COIL);
    }

    @Override
    public boolean isWriteExecutor() {
        return true;
    }

    @Override
    public Object adoptValue(Object initial) {
        String stringVal = String.valueOf(initial);
        Object res  = false;
        if (stringVal.equalsIgnoreCase("on") || stringVal.equalsIgnoreCase("true") || stringVal.equalsIgnoreCase("1")){
            res = true;
        }
        return res;
    }

    @Override
    protected ExecutionResult readData(ModbusResponse response, DeviceOperation deviceOperation) {
        try {
            WriteCoilResponse resp = (WriteCoilResponse) response;
            return positive(resp.getCoil());
        } catch (Exception e) {
            return fail(response, e);
        }

    }

    @Override
    public ModbusRequest createRequest(DeviceOperation operation) {
            return new WriteCoilRequest(operation.getStartRead(), getBool(operation.getValue()));
    }

    public static boolean getBool(Object value) {
        if (value == null) return false;

        String val = String.valueOf(value);
        try {
            double db = Double.parseDouble(val);
            return db > 0;
        } catch (Exception e) {
            if ("true".equalsIgnoreCase(val) || "on".equalsIgnoreCase(val) || "power on ".equalsIgnoreCase(val)) {
                return true;
            }
        }

        return false;
    }
}
