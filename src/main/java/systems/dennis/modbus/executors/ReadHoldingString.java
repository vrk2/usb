package systems.dennis.modbus.executors;

import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.msg.ReadMultipleRegistersRequest;
import com.ghgande.j2mod.modbus.msg.ReadMultipleRegistersResponse;
import systems.dennis.modbus.Modbus;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

public class ReadHoldingString extends AbstractExecutor {
    public static ReadHoldingString INSTANCE = new ReadHoldingString(null);
    public ReadHoldingString(Object value) {
        super(value);
    }

    @Override
    public boolean accept(DeviceOperation deviceOperation) {
        return deviceOperation.getType() == Modbus.READ_HOLDING_REGISTERS_STRING;
    }

    @Override
    public boolean isWriteExecutor() {
        return false;
    }


    @Override
    protected ExecutionResult readData(ModbusResponse response, DeviceOperation deviceOperation) {
        try {
            ReadMultipleRegistersResponse resp = (ReadMultipleRegistersResponse) response;
            return positive(toNormalString(resp.getMessage()));
        } catch (Exception e) {
            return fail(response, e);
        }

    }

    @Override
    public ModbusRequest createRequest(DeviceOperation operation) {
        return new ReadMultipleRegistersRequest(operation.getStartRead(), operation.getCount());
    }

    private String toNormalString(byte[] bytes) {
        var str = new StringBuffer();
        for (byte b : bytes) {

            if (b != 0) {
                str.append((char) b);
            }
        }
        return str.toString();
    }


}
