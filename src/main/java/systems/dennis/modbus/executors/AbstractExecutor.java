package systems.dennis.modbus.executors;

import com.ghgande.j2mod.modbus.ModbusException;
import com.ghgande.j2mod.modbus.io.ModbusSerialTransaction;
import com.ghgande.j2mod.modbus.msg.ModbusRequest;
import com.ghgande.j2mod.modbus.msg.ModbusResponse;
import com.ghgande.j2mod.modbus.net.SerialConnection;
import lombok.extern.slf4j.Slf4j;
import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;
import systems.dennis.modbus.controller.USBConnector;
import systems.dennis.shared.config.WebContext;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

import java.text.MessageFormat;

@Slf4j
public abstract class AbstractExecutor  implements Executor{

    private Object value;

    public AbstractExecutor(Object value){

        this.value = adoptValue(value);
    }

    @Override
    public ExecutionResult execute(DeviceOperation deviceOperation, WebContext.LocalWebContext context, USBConnector connector) {
        var nano = System.currentTimeMillis();
        if (deviceOperation == null) {
            return ExecutionResult.fail("operation was not found");
        }



        deviceOperation.setValue(value);
        var usb = deviceOperation.getDevice().getModel();
        SerialConnection connection;
        try {
            connection = connector.getConnection(usb);

        } catch (Exception e) {

            log.error("", e);
            return ExecutionResult.fail("cannot get connection for  " + usb.getName());
        }

        var request = createRequest(deviceOperation);
        request.setUnitID(deviceOperation.getDevice().getDeviceId().intValue());

        var transaction = new ModbusSerialTransaction(connection);
        transaction.setRequest(request);
        try {
            System.out.println("Start" + (System.currentTimeMillis() - nano));
            transaction.execute();
            System.out.println("executed" + (System.currentTimeMillis() - nano));
        } catch (ModbusException e) {
            log.error("", e);
            return ExecutionResult.fail("cannot execute transaction with the parameters " + e.getMessage());
        }

        try {
            return readData(transaction.getResponse(), deviceOperation);
        } catch (Exception e) {
            log.error("", e);
            return ExecutionResult.fail(" cannot read response: " + e.getMessage());
        } finally {
            System.out.println("END -" + (System.currentTimeMillis() - nano));
        }
    }


    public ExecutionResult fail(ModbusResponse response, Exception e) {
        ExecutionResult result = new ExecutionResult();
        result.setSuccess(false);
        result.setError( e.getMessage());
        result.setResult(null);
        return result;
    }

    public ExecutionResult positive(Object data) {
        ExecutionResult result = new ExecutionResult();
        result.setSuccess(true);
        result.setResult(data);
        return result;
    }

    public Object applyFormula(Object value, String formula) {

        if (value == null){
            return null;
        }

        if (value instanceof Number){
            if ("".equals(formula)){
                formula = "x";
            }
            Argument argument = new Argument("x", ((Number) value).intValue());
            var exp = new Expression(formula, argument);
            return exp.calculate();
        } else {
            if ("".equals(formula)){
                formula = "{0}";
            }
            return MessageFormat.format(formula, getValue());
        }

    }

    public Object getValue(){
        return value;
    }






    protected abstract ExecutionResult readData(ModbusResponse response, DeviceOperation deviceOperation);

    public abstract ModbusRequest createRequest(DeviceOperation operation);
}
