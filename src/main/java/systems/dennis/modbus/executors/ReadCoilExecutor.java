package systems.dennis.modbus.executors;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.msg.*;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

public class ReadCoilExecutor extends AbstractExecutor {
    public static ReadCoilExecutor INSTANCE = new ReadCoilExecutor(null);
    public ReadCoilExecutor(Object value) {
        super(value);
    }

    @Override
    public boolean accept(DeviceOperation deviceOperation) {
        return deviceOperation.getType().equals(Modbus.READ_COILS);
    }

    @Override
    public boolean isWriteExecutor() {
        return false;
    }


    @Override
    protected ExecutionResult readData(ModbusResponse response, DeviceOperation deviceOperation) {
        try {
            ReadCoilsResponse resp = (ReadCoilsResponse) response;
            return positive(resp.getCoilStatus(0));
        } catch (Exception e) {
            return fail(response, e);
        }

    }

    @Override
    public ModbusRequest createRequest(DeviceOperation operation) {
            return new ReadCoilsRequest(operation.getStartRead(), 1);
    }


}
