package systems.dennis.modbus.executors;

import com.ghgande.j2mod.modbus.msg.*;
import systems.dennis.modbus.Modbus;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

public class ReadHoldingNumber extends AbstractExecutor {

    public static ReadHoldingNumber INSTANCE = new ReadHoldingNumber(null);

    public ReadHoldingNumber(Object value) {
        super(value);
    }

    @Override
    public boolean accept(DeviceOperation deviceOperation) {
        return deviceOperation.getType() == Modbus.READ_HOLDING_REGISTERS;
    }

    @Override
    public boolean isWriteExecutor() {
        return false;
    }


    @Override
    protected ExecutionResult readData(ModbusResponse response, DeviceOperation deviceOperation) {
        try {
            ReadMultipleRegistersResponse resp = (ReadMultipleRegistersResponse) response;
            return positive(applyFormula(resp.getRegisterValue(0), deviceOperation.getFormula()));
        } catch (Exception e) {
            return fail(response, e);
        }

    }

    @Override
    public ModbusRequest createRequest(DeviceOperation operation) {
        return new ReadMultipleRegistersRequest(operation.getStartRead(), operation.getCount());
    }

    private Integer toNormalString(byte[] bytes) {
        var str = new StringBuffer();
        for (byte b : bytes) {
            str.append((int)b);
        }
        return Integer.parseInt(str.toString());
    }


}
