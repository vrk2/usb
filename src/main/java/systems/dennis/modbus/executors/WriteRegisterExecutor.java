package systems.dennis.modbus.executors;

import com.ghgande.j2mod.modbus.Modbus;
import com.ghgande.j2mod.modbus.msg.*;
import com.ghgande.j2mod.modbus.procimg.SimpleInputRegister;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

public class WriteRegisterExecutor extends AbstractExecutor {
    public static WriteRegisterExecutor INSTANCE = new WriteRegisterExecutor(null);
    public WriteRegisterExecutor(Object value) {
        super(value);
    }

    @Override
    public boolean accept(DeviceOperation deviceOperation) {
        return deviceOperation.getType().equals(Modbus.WRITE_SINGLE_REGISTER) || deviceOperation.getType().equals(Modbus.WRITE_MULTIPLE_REGISTERS);
    }

    @Override
    public boolean isWriteExecutor() {
        return true;
    }


    @Override
    protected ExecutionResult readData(ModbusResponse response, DeviceOperation deviceOperation) {
        try {
            WriteSingleRegisterResponse resp = (WriteSingleRegisterResponse) response;
            return positive(resp.getRegisterValue());
        } catch (Exception e) {
            return fail(response, e);
        }

    }

    @Override
    public ModbusRequest createRequest(DeviceOperation operation) {
        return new WriteSingleRegisterRequest(operation.getStartRead(),
                new SimpleInputRegister(Integer.parseInt(operation.getValue().toString())));
    }


}
