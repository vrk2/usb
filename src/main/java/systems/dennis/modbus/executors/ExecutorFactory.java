package systems.dennis.modbus.executors;

import lombok.SneakyThrows;
import systems.dennis.modbus.controller.USBConnector;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.pojo_form.ItemValue;
import systems.dennis.modbus.Modbus;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ExecutorFactory {
    public static final Map<Integer, AbstractExecutor> executors= new HashMap<>();
    private static final Map<Integer, Integer> reverseExecutors = new HashMap<>();
    public static final int NO_REVERSE = -9999;

    static  {
        registerExecutor(ReadHoldingNumber.INSTANCE, Modbus.READ_HOLDING_REGISTERS);
        registerExecutor(ReadHoldingString.INSTANCE, Modbus.READ_HOLDING_REGISTERS_STRING);
        registerExecutor(ReadCoilExecutor.INSTANCE, Modbus.READ_COILS);
        registerExecutor(WriteCoilExecutor.INSTANCE, Modbus.WRITE_COIL);
        registerExecutor(ReadDiscreteExecutor.INSTANCE, Modbus.READ_INPUT_DISCRETES);
        registerExecutor(ReadDiscreteExecutor.INSTANCE, Modbus.READ_INPUT_DISCRETES);
        registerExecutor(ReadRegisterExecutor.INSTANCE, Modbus.READ_REGISTER);
        registerExecutor(WriteRegisterExecutor.INSTANCE, Modbus.WRITE_SINGLE_REGISTER);
        registerExecutor(WriteHolding.INSTANCE, Modbus.WRITE_HOLDING);
        registerExecutor(WriteRegisterExecutor.INSTANCE, Modbus.WRITE_MULTIPLE_REGISTERS);

        reverseExecutors.put(Modbus.READ_HOLDING_REGISTERS, Modbus.WRITE_MULTIPLE_REGISTERS);
//        reverseExecutors.put(Modbus.WRITE_MULTIPLE_REGISTERS, Modbus.READ_HOLDING_REGISTERS);
        reverseExecutors.put(Modbus.READ_COILS, Modbus.WRITE_COIL);
//        reverseExecutors.put(Modbus.WRITE_COIL, Modbus.READ_COILS);
        reverseExecutors.put(Modbus.READ_INPUT_DISCRETES, Modbus.WRITE_COIL);
        reverseExecutors.put(Modbus.READ_REGISTER, Modbus.WRITE_SINGLE_REGISTER);
//        reverseExecutors.put(Modbus.WRITE_SINGLE_REGISTER, Modbus.READ_REGISTER);

    }

    public static Integer getReverseType(Integer originalType){
        if (originalType == null){
            return NO_REVERSE;
        }

        var type = reverseExecutors.get(originalType);

        return type == null? NO_REVERSE : type;
    }



    private static void registerExecutor(AbstractExecutor executor, int code){
        executors.put(code, executor);
    }

    public static ArrayList<ItemValue<String>> getExecutors(WebContext.LocalWebContext context){
        var res = new ArrayList<ItemValue<String>>();
        for (int val : executors.keySet()){
            res.add(new ItemValue<>(context.getMessageTranslation(executors.get(val).getClass().getSimpleName()), (long) val));
        }

        return res;
    }


    public static boolean typeIsWritable(int type){
        return executors.get(type).isWriteExecutor();
    }


    @SneakyThrows
    public static ExecutionResult execute(DeviceOperation operation, WebContext.LocalWebContext context){
        var executor = executors.get(operation.getType());
        if (!executor.accept(operation)){
            throw new IllegalArgumentException("Device operation type is not accepted by Executor, please check configuration");
        }


        // here we need to create a new instance with value inside, that's why we use this constructor
        var newExecutor = executor.getClass().getConstructor(Object.class).newInstance(operation.getValue());
        return newExecutor.execute(operation, context, context.getBean(USBConnector.class)  );
    }
}
