package systems.dennis.modbus.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import systems.dennis.shared.config.MessageResourceSource;
@RestController
@RequestMapping("/api/v1/messages")
@Slf4j
public class RefreshTranslations {

    final
    MessageResourceSource source;
    public RefreshTranslations(MessageResourceSource source) {
        this.source = source;
    }

    @GetMapping("/reload")
    public String refresh (){
        source.refresh();
        return "OK";
    }
}
