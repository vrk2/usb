package systems.dennis.modbus.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import systems.dennis.modbus.client.VirtualDeviceClient;
import systems.dennis.modbus.cron.Cron;
import systems.dennis.modbus.executors.ExecutorFactory;
import systems.dennis.modbus.model.OperationExecutionResult;
import systems.dennis.modbus.repo.OperationExecutionResultRepo;
import systems.dennis.shared.config.WebContext;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.ExecutionResult;
import systems.dennis.modbus.service.DeviceOperationService;

@RestController(value = "execution")
@RequestMapping("/api/v1/operation")
@Slf4j
public class ExecuteDeviceOperationController {

    private final WebContext.LocalWebContext context;
    private final DeviceOperationService operationService;
    private final OperationExecutionResultRepo operationExecutionResultRepo;
    private final VirtualDeviceClient virtualDeviceClient;

    public ExecuteDeviceOperationController(DeviceOperationService operationService, WebContext context,
                                            OperationExecutionResultRepo operationExecutionResultRepo, VirtualDeviceClient virtualDeviceClient) {

        this.operationService = operationService;
        this.context = WebContext.LocalWebContext.of("device.execution.controller", context);
        this.operationExecutionResultRepo = operationExecutionResultRepo;
        this.virtualDeviceClient = virtualDeviceClient;
    }


    @GetMapping("/execute/{id}")
    public ExecutionResult getLastExecution(@PathVariable("id") Long operationId) {
        OperationExecutionResult res = operationExecutionResultRepo.getLastExecution(operationId).orElse(new OperationExecutionResult());
        ExecutionResult r = new ExecutionResult();
        r.setSuccess(res.getError() != null && res.getError());
        r.setResult(res.getResult());
        r.setError(res.getErrorText());
        return r;
    }

    @GetMapping("/execute_write/{id}")
    public ExecutionResult runWithParam(@PathVariable("id") Long operationId, @RequestParam("value") String value) {

        return execute(operationId, value);
    }

    @PostMapping("/refresh/remote")
    public void refreshRemote() {
        context.getBean(Cron.class).runInRemote();
    }

    public String findDeviceValue(Long deviceId) {
        var item = operationService.findDefaultDeviceOperation(deviceId);
        if (!item.isPresent()) return context.getMessageTranslation("device_operation_not_found");
        return String.valueOf(getLastExecution(item.get().getId()).getResult());
    }


    public ExecutionResult execute(Long operationId, Object value) {
        var operation = operationService.findById(operationId).orElse(null);
        return execute(operation, value);
    }

    public ExecutionResult execute(DeviceOperation operation, Object value) {
        operation.setValue(value);

        if (operation.getDevice().isVirtual()) {
            ExecutionResult result = new ExecutionResult();
            result.setSuccess(true);
            try {
                result.setResult(virtualDeviceClient.execute(operation.getDevice(), operation));
                result.setSuccess(true);
            } catch (Exception e) {
                result.setResult(e.getMessage());
                result.setError(e.getMessage());
                result.setSuccess(false);

            }
            return result;
        }


        return ExecutorFactory.execute(operation, context);
    }


}
