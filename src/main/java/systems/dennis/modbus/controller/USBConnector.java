package systems.dennis.modbus.controller;

import com.ghgande.j2mod.modbus.net.SerialConnection;
import com.ghgande.j2mod.modbus.util.SerialParameters;
import org.springframework.stereotype.Service;
import systems.dennis.modbus.model.USBModusModel;

import jakarta.annotation.PreDestroy;
import jakarta.persistence.Transient;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class USBConnector {

    private final Map<String, SerialConnection > serialConnectionMap = new HashMap<>();

    public void clearConnections(){
        serialConnectionMap.clear();
    }

    public SerialConnection getConnection(USBModusModel connector){
        if (this.serialConnectionMap.get(connector.getName()) != null){
            return serialConnectionMap.get(connector.getName());
        }

        var connection = connect(connector);
        try {
            connection.open();
        } catch (IOException e) {
            connection = new InvalidConnection();
        }

        if (connection.isOpen()) {
            serialConnectionMap.put(connector.getName(), connection);
        }

        return connection;
    }

    @PreDestroy
    public  void disconnectAll(){
        for (String name : serialConnectionMap.keySet()){
            var con = serialConnectionMap.get(name);
            if (con instanceof InvalidConnection){
                continue;
            }
            try {
                con.close();
            } catch (Exception e){
                //nothing happens here
            }
        }
    }

    public static class InvalidConnection extends SerialConnection {
        @Override
        public synchronized boolean isOpen() {
            return false;
        }
    }

    @Transient
    private SerialConnection connect(USBModusModel model) {
        SerialParameters parameters = new SerialParameters();
        parameters.setBaudRate(model.getBaudRate());
        parameters.setDatabits(model.getDataBits());
        parameters.setStopbits(model.getStopBids());
        parameters.setRs485Mode(model.getIsPRCmode());
        parameters.setPortName(model.getName());
        parameters.setParity(model.getParity());
        return new SerialConnection(parameters);
    }

}
