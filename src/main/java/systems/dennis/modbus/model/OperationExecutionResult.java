package systems.dennis.modbus.model;

import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import java.util.Date;

@Data @Entity
public class OperationExecutionResult extends BaseEntity {
    private Date created = new Date();

    private Long deviceId;
    private Long operationId;
    @Column(columnDefinition = "varchar(1000)")
    private String result;
    private Boolean error;
    @Column (columnDefinition = "text")
    private String errorText;
    private Long modbusId;
}
