package systems.dennis.modbus.model;

import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;

import jakarta.persistence.Entity;

@Entity
@Data
public class Server extends BaseEntity {
    private String name;
    private String path;
    private String userName;
    private String password;

    @Override
    public String asValue() {
        return name;
    }
}
