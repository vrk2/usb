package systems.dennis.modbus.model;

import lombok.Data;
import lombok.Getter;
import systems.dennis.shared.entity.BaseEntity;

import java.util.List;

@Data
//@Entity
public class DeviceWidget extends BaseEntity {
    @Getter
    private Device device;

    private List<DeviceOperation> operationsToAdd;
    private Long id;
}
