package systems.dennis.modbus.model;

import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;

//@Entity
@Data
public class DeviceOperationPosition  extends BaseEntity {
    private DeviceOperation operation;
    private int position;
}
