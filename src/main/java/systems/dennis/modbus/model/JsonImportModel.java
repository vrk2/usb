package systems.dennis.modbus.model;

import lombok.Data;

import java.util.List;

@Data
public class JsonImportModel {
    private List<DeviceType> types;
    private List<DeviceTemplate> templates;
    private List<USBModusModel> usbs;
}
