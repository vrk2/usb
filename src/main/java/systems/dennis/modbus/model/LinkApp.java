package systems.dennis.modbus.model;

import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.utils.bean_copier.DataTransformer;
import systems.dennis.shared.utils.bean_copier.IdToObjectTransformer;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import java.util.Date;

@Entity
@Data
public class LinkApp extends BaseEntity {
    private String name;
    private String selfCode;
    private Date created;
    private String login;
    private String password;
    @OneToOne
    @DataTransformer(transFormWith = IdToObjectTransformer.class)
    private Server server;

    @OneToOne
    @DataTransformer(transFormWith = IdToObjectTransformer.class)
    private Server auth;

    @OneToOne
    @DataTransformer(transFormWith = IdToObjectTransformer.class)
    private Server central;
    @OneToOne
    @DataTransformer(transFormWith = IdToObjectTransformer.class)
    private Server virtual;

    @Column (name = "is_on")
    private Boolean on;

    @Column (nullable = true)
    private Boolean active;

}
