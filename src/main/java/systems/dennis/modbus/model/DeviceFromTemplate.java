package systems.dennis.modbus.model;

import lombok.Data;
import systems.dennis.shared.annotations.Validation;
import systems.dennis.shared.pojo_form.PojoFormElement;
import systems.dennis.shared.validation.ValueIsInt;


import java.io.Serializable;

@Data
public class DeviceFromTemplate implements Serializable {
    private Long usbId;
    private Long templateId;
    private String deviceName;
    @Validation(ValueIsInt.class)
    @PojoFormElement
    private Integer deviceId;

    public DeviceFromTemplate(Long usbId){

        this.usbId = usbId;
    }

}
