package systems.dennis.modbus.model;

import lombok.Data;

@Data
public class  ExecutionResult {
    private Object result;
    private String error;
    private boolean success;

    public static ExecutionResult fail(String s) {
        ExecutionResult res = new ExecutionResult();
        res.success = false;
        res.result = s;
        return res;
    }
}
