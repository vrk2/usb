package systems.dennis.modbus.model;

import lombok.Data;
import lombok.SneakyThrows;
import systems.dennis.modbus.service.DeviceOperationService;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.entity.DefaultForm;
import systems.dennis.shared.utils.bean_copier.DataTransformer;
import systems.dennis.shared.utils.bean_copier.IdToObjectTransformer;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Transient;

@Data

@Entity
public class DeviceOperation extends BaseEntity implements Cloneable {
    @OneToOne
    @DataTransformer(transFormWith = IdToObjectTransformer.class, additionalClass = DeviceOperationService.class)
    private Device device;
    private String name;
    private Integer type;
    private Boolean readOnly = Boolean.TRUE;
    private Boolean defaultOperation = Boolean.FALSE;
    private Integer startRead;
    private Integer count;
    private String formula;
    private Boolean defaultRead;
    private Boolean remoteEnable;
    private Long remoteId;

    @OneToOne
    @DataTransformer(transFormWith = IdToObjectTransformer.class, additionalClass = DeviceOperationService.class)
    private DeviceOperation reverseOperation;
    private boolean hasParameter;
    @Transient
    private Object value;

    @SneakyThrows
    public DeviceOperation clone(){
        return (DeviceOperation) super.clone();
    }

    public String toString(){
        return name + " type: " + type + " formula " + formula ;
    }

    public boolean hasReverseItems(){
        return  reverseOperation != null;
    }


    @Override
    public String asValue() {
        return name;
    }

}
