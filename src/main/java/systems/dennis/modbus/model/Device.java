package systems.dennis.modbus.model;

import lombok.Data;
import systems.dennis.modbus.service.DeviceTypeService;
import systems.dennis.modbus.service.UsbModusService;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.pojo_view.list.PojoListViewField;
import systems.dennis.shared.utils.bean_copier.DataTransformer;
import systems.dennis.shared.utils.bean_copier.IdToObjectTransformer;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Transient;

import java.util.List;

@Entity
@Data
public class Device extends BaseEntity {


    private Long deviceId;
    @OneToOne
    @DataTransformer(transFormWith = IdToObjectTransformer.class, additionalClass = UsbModusService.class)
    private USBModusModel model;

    private String deviceName;
    @OneToOne
    @DataTransformer(transFormWith = IdToObjectTransformer.class, additionalClass = DeviceTypeService.class)
    private DeviceType deviceType;

    private Long timeout;

    private Boolean virtual = false;

    private String path = null;
    private String pin = null;

    @Transient
    @PojoListViewField(searchable = false, visible = false)
    private List<DeviceOperation > operations;



    @Transient
    public boolean isVirtual(){
        return virtual ;
    }

    @Override
    public String asValue() {
        return deviceName;
    }
}
