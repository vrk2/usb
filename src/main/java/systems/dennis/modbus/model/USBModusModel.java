package systems.dennis.modbus.model;

import org.springframework.beans.BeanUtils;
import systems.dennis.modbus.forms.USBModusModelForm;
import systems.dennis.shared.pojo_form.FormTitle;
import systems.dennis.shared.pojo_form.PojoForm;
import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.pojo_view.list.PojoListView;

import jakarta.persistence.Entity;
import jakarta.persistence.Transient;

import java.util.List;

@Data
@Entity
@PojoForm(title = @FormTitle)
@PojoListView ()
public class USBModusModel extends BaseEntity {

    private String name;

    private String parity;

    private Integer dataBits;


    private Integer baudRate;


    private Integer stopBids;


    private Boolean isPRCmode;


    @Transient

    private List<Device> devices;

    @Transient
    private List<DeviceFromTemplate> fromTemplates;


    public static USBModusModel from(USBModusModelForm form) {
        USBModusModel m = new USBModusModel();
        BeanUtils.copyProperties(form, m);
        return m;
    }

    @Override
    public String asValue() {
        return name + "[" + baudRate + "->" + stopBids + "->" + parity+ "]";
    }
}
