package systems.dennis.modbus.model;

import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;

import jakarta.persistence.Entity;

@Data @Entity
public class DeviceType extends BaseEntity {
    private String name;
    private String templateName;

    @Override
    public String asValue() {
        return name;
    }
}
