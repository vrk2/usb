package systems.dennis.modbus.model;

import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;
import systems.dennis.shared.pojo_view.list.PojoListViewField;

import jakarta.persistence.Entity;
import jakarta.persistence.Transient;
import java.util.List;

@Data @Entity
public class DeviceTemplate extends BaseEntity  {
    private String templateName;

    private Integer deviceType;

    private Long timeout;

    @Transient
    @PojoListViewField(searchable = false, visible = false)
    private List<DeviceTemplateOperation> operations;
}
