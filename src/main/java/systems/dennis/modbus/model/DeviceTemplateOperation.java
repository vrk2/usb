package systems.dennis.modbus.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;

@Data
@Entity
public class DeviceTemplateOperation extends BaseEntity {
    @OneToOne

    private DeviceTemplate deviceTemplate;

    private String name;
    private Integer type;
    private Boolean readOnly = Boolean.TRUE;
    private Boolean defaultOperation = Boolean.FALSE;
    private Integer startRead;
    private Integer count;
    private String formula;
    private Boolean defaultRead;
    @JsonProperty("write")
    private boolean hasParameter;
}
