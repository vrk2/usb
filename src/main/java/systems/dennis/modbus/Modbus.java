package systems.dennis.modbus;

public interface Modbus  {
    /**
     * Defines the class 1 function code
     * for read coils.
     */
    int READ_COILS = 1;

    /**
     * Defines a class 1 function code
     * for read input discretes.
     */
    int READ_INPUT_DISCRETES = 2;

    /**
     * Defines a class 1 function code
     * for read holding registers
     */
    int READ_HOLDING_REGISTERS = 3;
    int READ_HOLDING_REGISTERS_STRING = -3;
    int WRITE_COIL = 5;
    int WRITE_SINGLE_REGISTER = 6;
    int WRITE_MULTIPLE_REGISTERS = 16;
    int WRITE_HOLDING = -16;
    int READ_REGISTER = 4;

}
