package systems.dennis.modbus.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import systems.dennis.shared.repository.PaginationRepository;
import systems.dennis.modbus.model.DeviceTemplateOperation;

import java.util.List;

@Repository
public interface DeviceTemplateOperationRepo extends PaginationRepository<DeviceTemplateOperation> {

    @Query(value = "update device_template_operation set default_operation = false  where device_template_id = :id" , nativeQuery = true)
    void  setAllMainOperationsToFalse(@Param("id") Long deviceId);

    @Query (value = "select * from  device_template_operation where device_template_id = :id", nativeQuery = true)
    List<DeviceTemplateOperation> getDeviceOperations(@Param("id") Long deviceTemplateId);
}
