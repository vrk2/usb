package systems.dennis.modbus.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import systems.dennis.modbus.model.Device;
import systems.dennis.shared.repository.PaginationRepository;

@Repository
public interface DeviceRepo extends PaginationRepository<Device> {

    @Query(value = "select case when count(0)> 0 then true else false end from device where model_id = :usb and device_id = :deviceId" , nativeQuery = true)
    boolean existsByDeviceIdAndModel(@Param("deviceId") long deviceId, @Param("usb") long usb);
}
