package systems.dennis.modbus.repo;

import org.springframework.stereotype.Repository;
import systems.dennis.modbus.model.USBModusModel;
import systems.dennis.shared.repository.PaginationRepository;

@Repository
public interface UsbModusRepo extends PaginationRepository<USBModusModel> {
}
