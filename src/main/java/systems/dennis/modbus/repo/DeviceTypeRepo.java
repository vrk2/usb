package systems.dennis.modbus.repo;

import org.springframework.stereotype.Repository;
import systems.dennis.shared.repository.PaginationRepository;
import systems.dennis.modbus.model.DeviceType;

@Repository
public interface DeviceTypeRepo extends PaginationRepository<DeviceType> {

}
