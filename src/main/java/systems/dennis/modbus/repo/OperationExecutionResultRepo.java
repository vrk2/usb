package systems.dennis.modbus.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import systems.dennis.shared.repository.PaginationRepository;
import systems.dennis.modbus.model.OperationExecutionResult;

import java.util.Optional;

@Repository
public interface OperationExecutionResultRepo extends PaginationRepository<OperationExecutionResult> {
    @Query (nativeQuery = true, value = "select * from  operation_execution_result where  operation_id=:id order by created desc limit 1 ")
    Optional<OperationExecutionResult> getLastExecution(@Param("id") Long operationId);
}
