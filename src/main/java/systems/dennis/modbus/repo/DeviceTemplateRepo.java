package systems.dennis.modbus.repo;

import org.springframework.stereotype.Repository;
import systems.dennis.modbus.model.DeviceTemplate;
import systems.dennis.shared.repository.PaginationRepository;

@Repository
public interface DeviceTemplateRepo extends PaginationRepository<DeviceTemplate> {
}
