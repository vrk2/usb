package systems.dennis.modbus.repo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.shared.repository.PaginationRepository;

import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface
DeviceOperationRepo extends PaginationRepository<DeviceOperation> {

    @Query("update DeviceOperation set defaultOperation = false  where device.id = :id")
    @Transactional
    @Modifying
    void  setAllMainOperationsToFalse(@Param("id") Long deviceId);

    @Query(value = " select * from device_operation  where device_id = :id and default_operation = true limit 1", nativeQuery = true)
    Optional<DeviceOperation> findDefaultDeviceOperation(@Param("id") Long deviceId);

    @Query (value = "select * from device_operation t where t.id not in (select f.reverse_operation_id from  device_operation f where f.reverse_operation_id is not null) and device_id = :id order by start_read ", nativeQuery = true)
    List<DeviceOperation> findOperations(@Param("id") Long deviceId);


    @Query (value = " select * from device_operation  where id = :id ", nativeQuery = true)
    Optional<DeviceOperation> findByReverseOperation(@Param("id") long reverseId);

    @Modifying
    @Transactional
    @Query (value = " update device_operation set reverse_operation_id = :id where id = :opId " , nativeQuery = true)
    void updateReverseOperationId(@Param("id") long reverseId, @Param("opId") Long operationId);

    Optional<DeviceOperation> findFirstByRemoteId(Long remoteId);

}

