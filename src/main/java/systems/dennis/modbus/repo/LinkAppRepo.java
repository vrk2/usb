package systems.dennis.modbus.repo;

import systems.dennis.modbus.model.LinkApp;
import systems.dennis.modbus.model.Server;
import systems.dennis.shared.repository.PaginationRepository;

import java.util.Optional;

public interface LinkAppRepo extends PaginationRepository<LinkApp> {
    Optional<LinkApp> findFirstByActiveIsTrue();

    Optional<LinkApp> findFirstByCentralAndSelfCodeNotNull(Server server);
}
