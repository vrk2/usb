package systems.dennis.modbus.repo;

import org.springframework.stereotype.Controller;
import systems.dennis.modbus.model.Server;
import systems.dennis.shared.annotations.WebFormsSupport;
import systems.dennis.shared.repository.PaginationRepository;

@Controller

public interface ServerRepo  extends PaginationRepository<Server> {
}
