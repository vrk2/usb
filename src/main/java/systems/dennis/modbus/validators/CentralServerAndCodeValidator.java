package systems.dennis.modbus.validators;

import lombok.SneakyThrows;
import systems.dennis.modbus.forms.LinkAppForm;
import systems.dennis.modbus.service.LinkAppService;
import systems.dennis.modbus.service.ServerService;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.exceptions.ItemNotFoundException;
import systems.dennis.shared.validation.ValueValidator;
import systems.dennis.shared.pojo_form.ValidationResult;

public class CentralServerAndCodeValidator implements ValueValidator<LinkAppForm, Long> {
    @SneakyThrows
    @Override
    public ValidationResult validate(Class serviceClass, LinkAppForm element, String field, Long value, boolean edit, WebContext.LocalWebContext context) {

        var server = context.getBean(ServerService.class).findById(value).orElseThrow(()->new ItemNotFoundException(value));

        var serveredItem = context.getBean(LinkAppService.class).findByServer(server);

        if (serveredItem != null){
//            return ValidationResult.fail("central.server.config.already.exists");

        }


        return ValidationResult.PASSED;
    }
}
