package systems.dennis.modbus.validators;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.validation.ValueValidator;
import systems.dennis.shared.pojo_form.ValidationResult;
import systems.dennis.modbus.executors.ExecutorFactory;
import systems.dennis.modbus.forms.DeviceTemplateOperationForm;

public class DefaultOperationNotWritableTemplate implements ValueValidator<DeviceTemplateOperationForm, Boolean> {
    @Override
    public ValidationResult validate(Class ser, DeviceTemplateOperationForm element, String field, Boolean value, boolean edit,  WebContext.LocalWebContext context) {
        if (ExecutorFactory.typeIsWritable(element.getType()) && value){
            //so we don't allow adding elements where writable as default display (because they don't have any display in request lifecycle
            return ValidationResult.fail(context.getMessageTranslation("writable_should_not_be_default"));
        }
        return ValidationResult.PASSED;
    }
}
