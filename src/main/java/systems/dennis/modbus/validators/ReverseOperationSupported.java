package systems.dennis.modbus.validators;

import systems.dennis.modbus.executors.ExecutorFactory;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.validation.ValueValidator;
import systems.dennis.shared.pojo_form.ValidationResult;
import systems.dennis.modbus.forms.DeviceOperationForm;

public class ReverseOperationSupported implements ValueValidator<DeviceOperationForm, Boolean> {
    @Override
    public ValidationResult validate(Class cl, DeviceOperationForm element, String field, Boolean value, boolean edit, WebContext.LocalWebContext context) {

        if (!value) return ValidationResult.PASSED;
        var isReverseExisting =  ExecutorFactory.getReverseType(element.getType());

        if (isReverseExisting != ExecutorFactory.NO_REVERSE){
            return ValidationResult.PASSED;
        }

        return ValidationResult.fail(context.getMessageTranslation("no_reverse_type_present"));
    }
}
