package systems.dennis.modbus.validators;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.validation.ValueValidator;
import systems.dennis.shared.pojo_form.ValidationResult;
import systems.dennis.modbus.forms.DeviceForm;
import systems.dennis.modbus.service.DeviceService;

public class NotExistingUsbDeviceValidator implements ValueValidator<DeviceForm, Long> {
    @Override
    public ValidationResult validate(Class cl, DeviceForm element, String field, Long value, boolean edit, WebContext.LocalWebContext context) {

        if (element.getId() != null){
            return ValidationResult.PASSED;
        }
        if (element.getModel() == null){
            return ValidationResult.PASSED;
        }
        if (element.getDeviceId() == null){
            return ValidationResult.PASSED;
        }
        var resolver = context.getBean(DeviceService.class);

        if (element.getDeviceId() == null){
            return ValidationResult.fail("device.validation.device_id.is_empty");
        }

        boolean res = resolver.existsByDeviceIdAndUsbModelId(element.getDeviceId(), element.getModel());
        if (!res){
            return ValidationResult.PASSED;
        } else {
            return ValidationResult.fail(context.getMessageTranslation("device.validation.id.already_exists"));
        }
    }
}
