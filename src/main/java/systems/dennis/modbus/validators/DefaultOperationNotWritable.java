package systems.dennis.modbus.validators;

import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.validation.ValueValidator;
import systems.dennis.shared.pojo_form.ValidationResult;
import systems.dennis.modbus.executors.ExecutorFactory;
import systems.dennis.modbus.forms.DeviceOperationForm;

public class DefaultOperationNotWritable implements ValueValidator<DeviceOperationForm, Boolean> {
    @Override
    public ValidationResult validate(Class serviceClass, DeviceOperationForm element, String field, Boolean value, boolean edit, WebContext.LocalWebContext context) {
        if (ExecutorFactory.typeIsWritable(element.getType()) && value){
            //so we don't allow to add elements where writable as default display (because they don't have any display in request lifecycle
            return ValidationResult.fail(context.getMessageTranslation("writable_should_not_be_default"));
        }
        return ValidationResult.PASSED;
    }
}
