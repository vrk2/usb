package systems.dennis.modbus.validators;

import systems.dennis.modbus.forms.USBModusModelForm;
import systems.dennis.shared.config.WebContext;
import systems.dennis.shared.validation.ValueValidator;
import systems.dennis.shared.pojo_form.ValidationResult;
import systems.dennis.modbus.service.UsbModusService;

public class NonExisitingUsbNameValidator implements ValueValidator<USBModusModelForm, String> {
    @Override
    public ValidationResult validate(Class cl, USBModusModelForm element, String field, String value, boolean edit, WebContext.LocalWebContext context) {
        if (element.getId() != null){
            return ValidationResult.PASSED;
        }
        var resolver = context.getBean(UsbModusService.class);

        boolean res =  resolver.existsByName(value);

        if (!res){
            return ValidationResult.PASSED;
        } else {
            return ValidationResult.fail(context.getMessageTranslation("usb.model.name.already.exists"));
        }
    }
}
