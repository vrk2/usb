package systems.dennis.modbus.cron;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import systems.dennis.modbus.client.CentralAppClient;
import systems.dennis.modbus.client.model.DeviceOperationModel;
import systems.dennis.modbus.model.ExecutionResult;
import systems.dennis.shared.repository.DefaultSearchSpecification;
import systems.dennis.shared.repository.QueryCase;
import systems.dennis.modbus.controller.ExecuteDeviceOperationController;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.OperationExecutionResult;
import systems.dennis.modbus.repo.OperationExecutionResultRepo;
import systems.dennis.modbus.service.DeviceOperationService;
import systems.dennis.modbus.service.DeviceService;

import java.util.ArrayList;
import java.util.List;



@Service
@Slf4j
@Scope ("singleton")
public class Cron {

    private final DeviceService service;
    private final DeviceOperationService operationService;
    private final ExecuteDeviceOperationController controller;
    private final OperationExecutionResultRepo operationExecutionResultRepo;
    private final CentralAppClient centralAppClient;
    boolean inRun;
    private boolean sendRemoteAnyWay = false;

    public Cron(DeviceService service, DeviceOperationService operationService,
                ExecuteDeviceOperationController controller, OperationExecutionResultRepo operationExecutionResultRepo, CentralAppClient centralAppClient) {
        this.service = service;
        this.operationService = operationService;
        this.controller = controller;
        this.operationExecutionResultRepo = operationExecutionResultRepo;
        this.centralAppClient = centralAppClient;
    }

//    @Scheduled(fixedRateString = "${usb.cron.read_timeout:5000}")
    public void runNews(){
        var res = centralAppClient.readNews();
        if (res == null || res.isEmpty()){
            return;
        }
        List<DeviceOperationModel> dom = new ArrayList<>();
        for (DeviceOperationModel model : res){

            var op = operationService.findByRemoteId(model.getId());
            if (op == null){
                log.trace("We could not find operartion with the remote id: " + model.getRemoteId());
                continue;
            }
            var value = model.getValue();



            op.setValue(value);
            executeOperation(op, dom);

        }

    }

    public void runInRemote(){
        this.sendRemoteAnyWay = true;
    }

//    @Scheduled(fixedRateString = "${usb.cron.read_timeout:5000}")
    public void run() {

        try {
            if (inRun ) {
                log.info(" Already in run ");
                return;
            }
            inRun = true;

            var res= service.find();

            for (Device device : res){
                List<DeviceOperationModel> modelList = new ArrayList<>();
                QueryCase exp = QueryCase.equalsOf("id", device.getId());
                exp.setJoinOn("device");
                exp.setComplex(true);
                DefaultSearchSpecification<DeviceOperation> spec = exp.specification();
                spec.addCase(QueryCase.equalsOf("hasParameter", false));
                var ops = operationService.getRepository().findAll(spec);

                for (DeviceOperation op : ops){
                   executeOperation(op, modelList);

                }
                centralAppClient.sendData(modelList);

            }

        } finally {
            inRun = false;
            sendRemoteAnyWay = false;
        }
    }

    private List<DeviceOperationModel> executeOperation(DeviceOperation op, List<DeviceOperationModel> modelList) {
        OperationExecutionResult result = new OperationExecutionResult();
        result.setDeviceId(op.getDevice().getId());
        result.setOperationId(op.getId());
        result.setModbusId(op.getDevice().getModel().getId());
        ExecutionResult operationRes = null;
        try {
            operationRes =  controller.execute(op.getId(), op.getValue());
            result.setError(false);
            result.setResult(String.valueOf(operationRes.getResult()));

        } catch (Exception e){

            result.setErrorText(e.getMessage());
            result.setError(true);
        } finally {
            try {
                var lastResult = operationExecutionResultRepo.getLastExecution(op.getId());
                if ( true) { //sendRemoteAnyWay || !(lastResult.isPresent() && operationRes != null && lastResult.get().getResult().equals(String.valueOf(operationRes.getResult())))) {
                    operationExecutionResultRepo.save(result);
                    if (op.getRemoteEnable()!=null && op.getRemoteEnable()) {
                        modelList.add(DeviceOperationModel.from(result, op, op.getDevice()));
                    }
                    log.info("Ok new data for execution");
                } else {
                    log.info("ignoring data, the same as it was");
                }
            } catch (Exception e){
                //ignored
            }

        }

        return modelList;
    }
}
