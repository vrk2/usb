package systems.dennis.modbus.client.model;

import lombok.Data;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.shared.entity.DefaultForm;

@Data
public class RemoteWritableForm implements DefaultForm {

    private Long id;
    private Long device;
    private String deviceName;
    private String operationName;
    private Long operationId;
    private String usbName;
    private Long usb;
    private String value;
    private Long deviceType;
    private Integer operationType;
    private String applicationId;
    private Long remoteId;


    public static RemoteWritableForm from(DeviceOperation result) {
        RemoteWritableForm model = new RemoteWritableForm();
        model.setDevice(result.getDevice().getId());
        model.setDeviceType(result.getDevice().getDeviceType().getId());
        model.setOperationName(result.getName());
        model.setOperationId(result.getId());
        model.setOperationType(result.getType());
        model.setUsb(result.getDevice().getModel().getId());
        model.setUsbName(result.getDevice().getModel().getName());
        model.setDeviceName(result.getDevice().getDeviceName());
        model.setRemoteId(result.getRemoteId());
        model.setUsb(result.getDevice().getModel().getId());
        model.setUsbName(result.getDevice().getModel().getName());
        return model;
    }
}
