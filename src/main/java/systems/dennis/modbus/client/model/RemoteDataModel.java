package systems.dennis.modbus.client.model;

import lombok.Data;

@Data
public class RemoteDataModel {
    private Long usb;
    private String usbName;
    private Long device;
    private String deviceName;
    private String operationName;
    private Long operationId;
    private String value;
    private Integer deviceType;
    private Integer operationType;
}
