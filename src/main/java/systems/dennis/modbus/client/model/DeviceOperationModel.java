package systems.dennis.modbus.client.model;

import lombok.Data;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.model.OperationExecutionResult;
import systems.dennis.usb.auth.role_validator.entity.UserAssignableEntity;

import jakarta .persistence.Entity;
import java.util.Date;

@Data
public class DeviceOperationModel extends UserAssignableEntity {
    private Long usb;
    private String usbName;
    private Long device;
    private String deviceName;
    private String operationName;
    private Long operationId;
    private String value;
    private Long deviceType;
    private Integer operationType;
    private Date date = new Date();
    private Long remoteId;

    private String applicationId;

    public static DeviceOperationModel from(OperationExecutionResult result, DeviceOperation op, Device device) {
        DeviceOperationModel model = new DeviceOperationModel();
        model.setDevice(result.getDeviceId());
        model.setDeviceType(device.getDeviceType().getId());
        model.setOperationName(op.getName());
        model.setOperationId(op.getId());
        model.setOperationType(op.getType());
        model.setUsb(device.getModel().getId());
        model.setUsbName(device.getModel().getName());
        model.setValue(result.getResult());
        model.setDeviceName(device.getDeviceName());
        model.setRemoteId(op.getRemoteId());
        return model;
    }


}
