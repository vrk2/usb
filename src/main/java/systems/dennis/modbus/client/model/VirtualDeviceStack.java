package systems.dennis.modbus.client.model;

import lombok.Data;
import systems.dennis.shared.entity.BaseEntity;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;

@Data
@Entity
public class VirtualDeviceStack extends BaseEntity {

    private String value = "";
    private int type;


    int start = 1;
    int bits = 1;

    Boolean allowWrite = false;
}
