package systems.dennis.modbus.client;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import systems.dennis.modbus.client.model.DeviceOperationModel;
import systems.dennis.modbus.client.model.RemoteWritableForm;
import systems.dennis.modbus.forms.LinkAppForm;
import systems.dennis.modbus.model.LinkApp;
import systems.dennis.modbus.service.LinkAppService;
import systems.dennis.shared.exceptions.AccessDeniedException;
import systems.dennis.shared.utils.bean_copier.BeanCopier;
import systems.dennis.usb.auth.client.remote.AuthClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class CentralAppClient {
    private final RestTemplate restTemplate;
    private final AuthClient authClient;

    private final LinkAppService appService;
    private BeanCopier copier;

    public CentralAppClient(RestTemplate restTemplate, AuthClient client, LinkAppService appService, BeanCopier copier) {
        this.restTemplate = restTemplate;
        this.authClient = client;
        this.appService = appService;
        this.copier = copier;
    }

    @SneakyThrows
    public RemoteWritableForm saveWritable(RemoteWritableForm form) {
        if (form.getRemoteId() != null) {
            form.setId(form.getRemoteId());
        }

        if (appService.getCurrentApp() == null || !appService.getCurrentApp().getOn()) {
            log.info("Current Application is not set, ignoring sending data. ");
            return null;
        }
        form.setApplicationId(appService.getCurrentApp().getSelfCode());
        var request = createHeaders(authClient.login(), form);

        String addEdit = form.getId() == null ? "add" : "edit";
        ResponseEntity<RemoteWritableForm> res = null;
        if (form.getId() != null){
            res = restTemplate.exchange(appService.getCurrentApp().getCentral().getPath() + "remote/data/write/" + addEdit, HttpMethod.PUT , request, RemoteWritableForm.class);
        } else {
            res = restTemplate.postForEntity(appService.getCurrentApp().getCentral().getPath()  + "remote/data/write/" + addEdit, request, RemoteWritableForm.class);
        }
        if (res.getStatusCode() == HttpStatus.FORBIDDEN) {
            throw new AccessDeniedException("User login password is incorrect");
        }

        return res.getBody();


    }

    public List<DeviceOperationModel> readNews(){

        if (appService.getCurrentApp() == null || !appService.getCurrentApp().getOn()) {
            log.trace("Current app is not installed, ignoring");
            return null;
        }
        var token = authClient.login();
        var request  = createHeaders(token, null );

        var res = restTemplate.exchange(appService.getCurrentApp().getCentral().getPath() + "/remote/data/write/get/" + appService.code(), HttpMethod.GET, request, DeviceOperationModel[].class);
        if (res.getStatusCode() == HttpStatus.FORBIDDEN) {
            throw new AccessDeniedException("User login password is incorrect");
        }
        if (res.getBody() == null){
            return new ArrayList<>();
        }
        return Arrays.asList(res.getBody());
    }

    @SneakyThrows
    public void sendData(List<DeviceOperationModel> model) {
        if (model.isEmpty()){
            return;
        }
        var token = authClient.login();


        if (appService.getCurrentApp() == null || !appService.getCurrentApp().getOn()) {
            log.info("Current Application is not set, ignoring sending data. ");
            return;
        }
        model.forEach(x -> x.setApplicationId(appService.code()));

        var request = createHeaders(token, model);

        var res = restTemplate.postForEntity(appService.getCurrentApp().getCentral().getPath() + "remote/data/add/multiple", request, DeviceOperationModel[].class);
        if (res.getStatusCode() == HttpStatus.FORBIDDEN) {
            throw new AccessDeniedException("User login password is incorrect");
        }
        if (res.getBody() == null) {
            throw new RuntimeException("not able to get result status: " + res.getStatusCode());
        }

    }

    private HttpEntity createHeaders(String token, Object model) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        HttpEntity request = new HttpEntity<>(model, headers);
        return request;
    }

    @SneakyThrows
    public LinkAppForm registerApp(LinkAppForm form) {

        var token = authClient.login();

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        HttpEntity<LinkAppForm> request = new HttpEntity<>(form, headers);

        LinkApp app = copier.copy(form, LinkApp.class);


        var res = restTemplate.postForEntity(app.getCentral().getPath() + "remote/add", request, LinkAppForm.class);
        if (res.getStatusCode() == HttpStatus.FORBIDDEN) {
            throw new AccessDeniedException("User login password is incorrect");
        }
        if (res.getBody() == null) {
            throw new RuntimeException("not able to get result status: " + res.getStatusCode());
        }
        return res.getBody();

    }


}
