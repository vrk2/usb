package systems.dennis.modbus.client;

import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import systems.dennis.modbus.Modbus;
import systems.dennis.modbus.client.model.VirtualDeviceStack;
import systems.dennis.modbus.model.Device;
import systems.dennis.modbus.model.DeviceOperation;
import systems.dennis.modbus.forms.VirtualDeviceForm;
import systems.dennis.modbus.service.LinkAppService;

@Service
public class VirtualDeviceClient {
    private final RestTemplate restTemplate;
    private final LinkAppService appService;


    public VirtualDeviceClient(RestTemplate restTemplate, LinkAppService appService) {
        this.restTemplate = restTemplate;

        this.appService = appService;
    }

    @SneakyThrows
    public VirtualDeviceForm connect(Device device) {

        var res = restTemplate.getForEntity(appService.getCurrentApp().getVirtual().getPath() + "/connect/" + device.getPath() + "/" + device.getPin(), VirtualDeviceForm.class);

        return res.getBody();


    }

    public String execute(Device device, DeviceOperation op) {
        String prefix = "/stack/";

        if (

                op.getType() == Modbus.READ_HOLDING_REGISTERS ||
                        op.getType() == Modbus.READ_REGISTER ||
                        op.getType() == Modbus.READ_COILS ||
                        op.getType() == Modbus.READ_INPUT_DISCRETES ||
                        op.getType() == Modbus.READ_HOLDING_REGISTERS_STRING
        ) {
            prefix += "read/";
        } else {
            prefix += "write/";
        }

        int type = 0;

        switch (op.getType()) {
            case Modbus.READ_HOLDING_REGISTERS:
            case Modbus.WRITE_HOLDING:
            case Modbus.READ_HOLDING_REGISTERS_STRING: {
                type = 4;
                break;
            }
            case Modbus.READ_REGISTER: {
                type = 3;
                break;
            }

            case Modbus.READ_INPUT_DISCRETES: {
                type = 2;
                break;
            }

            case Modbus.WRITE_COIL: {
                type = 1;
                break;
            }
        }

        String path = prefix + device.getPath() + "/" + device.getPin() + "/" + type + "/" + op.getStartRead() + "/" + op.getCount();

        if (op.getValue() != null){
            path += "/" + op.getValue();
        }

        return restTemplate.getForEntity(appService.getCurrentApp().getVirtual().getPath() + path, VirtualDeviceStack.class).getBody().getValue();

    }
}
