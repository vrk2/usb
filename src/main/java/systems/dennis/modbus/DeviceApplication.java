package systems.dennis.modbus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;
import systems.dennis.shared.annotations.security.ISecurityUtils;
import systems.dennis.shared.config.MessageResourceSource;
import systems.dennis.shared.entity.DefaultForm;

import java.time.Duration;

@EnableJpaRepositories({ "systems.dennis.modbus.*","systems.dennis.shared.*"})
@EntityScan({"systems.dennis.modbus.*","systems.dennis.shared.*"})
@SpringBootApplication(scanBasePackages = {
        "systems.dennis.modbus", "systems.dennis.usb.auth.client.remote" , "systems.dennis.shared.*",
})
@EnableTransactionManagement
@EnableScheduling
public class DeviceApplication {
    public static void main(String[] args) {
        SpringApplication.run(DeviceApplication.class);
    }


    @Autowired MessageResourceSource resourceSource;

    @Bean @Primary
    public MessageSource messageSource() {
        resourceSource.setBasename("messages");
        resourceSource.setDefaultEncoding("UTF-8");
        return resourceSource;
    }




    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {

        return builder
                .setConnectTimeout(Duration.ofMillis(3000))
                .setReadTimeout(Duration.ofMillis(3000))
                .build();
    }

    @Configuration
    public static class SpringSecurityConfiguration {

        AuthenticationManager authenticationManager;

        @Autowired
        UserDetailsService userDetailsService;

        @Bean
        public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

            AuthenticationManagerBuilder authenticationManagerBuilder = http.getSharedObject(AuthenticationManagerBuilder.class);
            authenticationManagerBuilder.userDetailsService(userDetailsService);
            authenticationManager = authenticationManagerBuilder.build();

            http.csrf().disable().cors().disable().authorizeHttpRequests().anyRequest().permitAll()
                    .and()
                    .authenticationManager(authenticationManager)
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
            return http.build();
        }

    }

    @Bean public ISecurityUtils getUtils(){
        return new ISecurityUtils() {
            @Override
            public Long getUserDataId() {
                return -1L;
            }

            @Override
            public void assignUser(DefaultForm pojo) {

            }

            @Override
            public void assignUser(DefaultForm form, DefaultForm pojoOriginal) {

            }

            @Override
            public String getToken() {
                return null;
            }

            @Override
            public boolean isAdmin() {
                return false;
            }

            @Override
            public boolean hasRole(String role) {
                return false;
            }

            @Override
            public String getUserLanguage() {
                return null;
            }
        };
    }
}
