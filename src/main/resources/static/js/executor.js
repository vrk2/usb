function execute(id, reader) {

    $.ajax({
        type: "GET",
        url: "/api/v1/operation/execute/" + id,
        success: function (res) {
            $("#" + reader).html(res.result);
        },
        error: function (e) {
            $.toast("Cannot run in a moment" + e.responseText);
        }
    });
}

    function executeWrite(id, reader){
    var value = window.prompt("What will be the value? ");
    if (value === undefined || value === ""){
        return;
    }
    $.ajax({
        type: "GET",
        url: "/api/v1/operation/execute_write/" + id + "?value=" + value,
        success: function (res) {
            $("#" +reader).html(res.result);
        },
        error: function (e) {
            $.toast("Cannot run in a moment" + e.responseText);
        }
    });
}

function ping(path, id) {
    let element = document.getElementById("server_" + id);
    $.ajax({
        type: "GET",
        url: path,
        success: function (res) {
            if (res == '200') {
                element.src = "/images/widgets/server/connected.png"
            } else {
                element.src = "/images/widgets/server/no_connection.png"
            }
        },
        error: function (e) {
            element.src = "/images/widgets/coil/off.png"
        }
    });

    setTimeout(function () {
        ping(path, id)
    }, 5000)

}

function executeWriteByType(id, val ,type){

    let onchange = function (value){
        $.ajax({
            type: "GET",
            url: "/api/v1/operation/execute_write/" + id + "?value=" + value,
            success: function (res) {
                $.toast(res.result);
            },
            error: function (e) {
                $.toast("Cannot run in a moment" + e.responseText);
            }
        });
    }
    startType(type, val, onchange )
}

function remove(object, path, toDelete) {
    let conf = confirm("Delete object " + object + "?");
    if (!conf){
        return;
    }
    $.ajax({
        type: "DELETE",
        url: "/api/v2/" + path + "/delete/" + object,
        success: function () {
            item = toDelete == null ? $("#cell_" + object) : toDelete;
            item.remove();
            $.toast("Success");
        },
        error: function (e) {
            $.toast("Cannot run in a moment" + e.responseText);
        }
    });

}
function refreshRemote() {
    let conf = confirm("Refresh data remotely? This will add data remotely with the save values if they have been there. \n Normally this used to update list of devices in the remote central app");
    if (!conf){
        return;
    }
    $.ajax({
        type: "POST",
        url: "/api/v1/operation/refresh/remote",
        success: function () {
            $.toast("Success");
        },
        error: function (e) {
            $.toast("Cannot run in a moment" + e.responseText);
        }
    });

}

function drawSingleNumber(item) {
    return  "<img width='24px' height='24px' src='/images/widgets/registers/" + item + ".png'/>" ;

}

function drawNumber(number, whereId) {

    var res = "";
    for (var i = 0; i < number.length; i++) {
        var item = number.charAt(i);

        if (item == '.' || item == ',') {
            item = "dot"
        }
        res += drawSingleNumber(item);
    }

    document.getElementById(whereId).innerHTML = res;
}

var tp = -1;

function startType(type, val, onChange) {



    if (type == 5) {
        if (val == null){
            val = false;
        }
        $("#modal_checkbox").attr("checked", val);


        $(".number").addClass("hidden")
        $(".bool").removeClass("hidden")
    } else {
        if (val == null) {
            val = '';
        }
        $(".bool").addClass("hidden")
        $(".number").removeClass("hidden")
        $("#number_value").val(val);
    }
    var btn = $("#btn_ok_modal");
    btn.prop("onclick", null).off("click");
    btn.click(function () {
        var chkbox = $("#modal_checkbox");
        var lastValue;
        if (tp === 1) {
            lastValue = chkbox.attr('checked');
        } else {
            var numberVal = $("#number_value");
            lastValue = numberVal.val();
        }
        onChange(lastValue);
    });

    var cont = $('#md_cnt_1').removeClass("hidden")

}